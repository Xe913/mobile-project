using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUGKeyboardInputHandler : MonoBehaviour
{
    [Range(0, 2)]
    [SerializeField] private float keyboardInputSensibility;

    private GlobalEventsHandler geh;
    private PlayerControls playerControls;
    private bool setup;
    private bool usingKeyboard;
    private bool shipControlsEngaged;
    private Vector2 keybordInput;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        playerControls = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<PlayerControls>();
        geh.Add_OnShipControlsEngaged(EnableKeyboardInput);
        geh.Add_OnRequestShipControlsDisengage(DisableKeyboardInput);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnShipControlsEngaged(EnableKeyboardInput);
            geh.Remove_OnRequestShipControlsDisengage(DisableKeyboardInput);
        }
    }

    public void Update()
    {
        if (shipControlsEngaged)
        {
            if (!setup)
            {
                setup = (playerControls.ControlScheme != null);
                if (setup) usingKeyboard = (playerControls.ControlScheme is KeyboardControlScheme);
            }
            if (usingKeyboard)
            {
                if (Input.GetKeyDown(KeyCode.W)) keybordInput.y += 1;
                if (Input.GetKeyUp(KeyCode.W)) keybordInput.y -= 1;
                if (Input.GetKeyDown(KeyCode.A)) keybordInput.x -= 1;
                if (Input.GetKeyUp(KeyCode.A)) keybordInput.x += 1;
                if (Input.GetKeyDown(KeyCode.S)) keybordInput.y -= 1;
                if (Input.GetKeyUp(KeyCode.S)) keybordInput.y += 1;
                if (Input.GetKeyDown(KeyCode.D)) keybordInput.x += 1;
                if (Input.GetKeyUp(KeyCode.D)) keybordInput.x -= 1;
            }
            geh.Invoke_OnShipRotationInput(keybordInput * keyboardInputSensibility);
        }
    }

    private void EnableKeyboardInput()
    {
        shipControlsEngaged = true;
    }

    private void DisableKeyboardInput()
    {
        shipControlsEngaged = false;
    }
}
