using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    SceneLoader sl;

    public void Start()
    {
        sl = GameObject.FindGameObjectWithTag("GameController").GetComponent<SceneLoader>();
    }

    public void StartGame()
    {
        sl.LoadMainMenu();
    }
}
