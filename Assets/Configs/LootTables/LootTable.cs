using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Loot Table", menuName = "Loot Table")]
[System.Serializable]
public class LootTable : ScriptableObject
{
    [SerializeField] private List<GameObject> _containedLoot;

    public List<GameObject> ContainedLoot { get => _containedLoot; }
}
