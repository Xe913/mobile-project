using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Const Dictionary", menuName = "Const Dictionary")]
[System.Serializable]
public class ConstDictionary : ScriptableObject
{
    [Header("Tags")]
    [SerializeField] private string _globalEventHandlerTag = "GlobalEventHandler";
    [SerializeField] private string _playerTag = "Player";
    [SerializeField] private string _shipTag = "Ship";
    [SerializeField] private string _worldTag = "World";
    [SerializeField] private string _treasureMapTag = "TreasureMap";
    [SerializeField] private string _stateInfoTag = "CurrentState";
    [SerializeField] private string _playerControlsCanvas = "PlayerControlsCanvas";
    [SerializeField] private string _shipControlsCanvas = "ShipControlsCanvas";
    [SerializeField] private string _audioController = "AudioController";
    [Header("Layer")]
    [SerializeField] private string _normalViewLayer = "Default";
    [SerializeField] private string _radarLayer = "Radar";
    [SerializeField] private string _shipLayer = "Ship";
    [SerializeField] private string _levelBoundsLayer = "LevelBound";
    [Header("Scene Names")]
    [SerializeField] private string _mainMenuSceneName = "MainMenu";
    [SerializeField] private string _levelScenePrefix = "Level";
    [SerializeField] private string _endingScreenSceneName = "EndingScreen";
    [SerializeField] private string _creditsSceneName = "Credits";
    [SerializeField] private string _testLevelSceneName = "TestLevel";
    [Header("Options")]
    [SerializeField] private string _difficultySettingsKey = "Difficulty";
    [SerializeField] private string _bgMusicVolumeSettingsKey = "bgMusicVolume";
    [SerializeField] private string _soundEffectsVolumeSettingsKey = "SoundEffectsVolume";
    [SerializeField] private DifficultySettings _defaultDifficultySetting = DifficultySettings.normal;
    [SerializeField] private float _defaultBgMusicVolume = 0.5f;
    [SerializeField] private float _defaultSoundEffectsVolume = 0.5f;
    [Header("Gameplay")]
    [SerializeField] private float _defaultTimeScale = 1f;

    //TAGS
    public string GlobalEventHandlerTag { get => _globalEventHandlerTag; }
    public string PlayerTag { get => _playerTag; }
    public string ShipTag { get => _shipTag; }
    public string WorldTag { get => _worldTag; }
    public string TreasureMapTag { get => _treasureMapTag; }
    public string StateInfoTag { get => _stateInfoTag; }
    public string PlayerControlsCanvas { get => _playerControlsCanvas; }
    public string ShipControlsCanvas { get => _shipControlsCanvas; }
    public string AudioController { get => _audioController; }

    //LAYERS
    public string NormalViewLayer { get => _normalViewLayer; }
    public string RadarLayer { get => _radarLayer; }
    public string ShipLayer { get => _shipLayer; }
    public string LevelBoundsLayer { get => _levelBoundsLayer; }

    //SCENE NAMES
    public string MainMenuSceneName { get => _mainMenuSceneName; }
    public string LevelScenePrefix { get => _levelScenePrefix; }
    public string EndingScreenSceneName { get => _endingScreenSceneName; }
    public string CreditsSceneName { get => _creditsSceneName; }
    public string TestLevelSceneName { get => _testLevelSceneName; }

    //OPTIONS
    public string DifficultySettingsKey { get => _difficultySettingsKey; }
    public string BgMusicVolumeSettingsKey { get => _bgMusicVolumeSettingsKey; }
    public string SoundEffectsVolumeSettingsKey { get => _soundEffectsVolumeSettingsKey; }
    public DifficultySettings DefaultDifficultySetting { get => _defaultDifficultySetting; }
    public float DefaultBgMusicVolume { get => _defaultBgMusicVolume; }
    public float DefaultSoundEffectsVolume { get => _defaultSoundEffectsVolume; }

    //GAMEPLAY
    public float DefaultTimeScale { get => _defaultTimeScale; }
}
