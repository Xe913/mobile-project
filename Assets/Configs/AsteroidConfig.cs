using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Asteroid Config", menuName = "Asteroid Config")]
[System.Serializable]
public class AsteroidConfig : ScriptableObject
{
    [SerializeField] private List<GameObject> _models;

    public List<GameObject> Models { get => _models; }
}
