using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUGFire : MonoBehaviour
{
    [SerializeField] private GameObject gunGO;
    [SerializeField] private GameObject beaconGO;
    [SerializeField] private GameObject magnetGO;

    private Gun gun;
    private Scanner beacon;
    private Magnet magnet;

    public void Start()
    {
        gun = gunGO.GetComponent<Gun>();
        beacon = beaconGO.GetComponent<Scanner>();
        magnet = magnetGO.GetComponent<Magnet>();

    }

    public void FireGun()
    {
        gun.StartFiring();
    }

    public void ReloadGun()
    {

    }

    public void FireBeacon()
    {
        Debug.Log("FIRE BEACON");
        beacon.StartFiring();
    }

    public void FireMagnet()
    {
        magnet.StartFiring();
    }
}
