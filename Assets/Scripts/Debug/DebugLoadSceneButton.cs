using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUGLoadSceneButton : MonoBehaviour
{
    private SceneLoader sl;

    public void Start()
    {
        sl = GameObject.FindGameObjectWithTag("GameController").GetComponent<SceneLoader>();
    }

    public void GoToMainMenu()
    {
        sl.LoadMainMenu();
    }

    public void GoToTestLevel()
    {
        sl.LoadTestLevel();
    }

    public void GoToEndingScreen()
    {
        sl.LoadEndingScreen();
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
