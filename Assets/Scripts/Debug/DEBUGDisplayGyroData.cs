using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DEBUGDisplayGyroData : MonoBehaviour
{
    private ConstDictionary constDict;

    private TextMeshProUGUI displayText;
    private GyroControlScheme gyro;

    void Start()
    {
        displayText = GetComponent<TextMeshProUGUI>();
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        PlayerControls selector = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<PlayerControls>();
        if(selector.ControlScheme is GyroControlScheme)
        {
            gyro = (GyroControlScheme)selector.ControlScheme;
        }
        else
        {
            displayText.text = "Can't Detect Gyro Control Scheme";
            Destroy(gameObject);
        }
    }

    void Update()
    {
        /*
        displayText.text = "player rotation\t" + String.Format("{0:0.0000}", gyro.ControlledObjectRotation.x) + "\t" +
            String.Format("{0:0.0000}", gyro.ControlledObjectRotation.y) + "\t" +
            String.Format("{0:0.0000}", gyro.ControlledObjectRotation.z) + "\n" +
            "default rotation\t" + String.Format("{0:0.0000}", gyro.DefaultRotation.x) + "\t" +
            String.Format("{0:0.0000}", gyro.DefaultRotation.y) + "\t" +
            String.Format("{0:0.0000}", gyro.DefaultRotation.z) + "\n" +
            "frame rotation\t" + String.Format("{0:0.0000}", gyro.frameRotation.x) + "\t" +
            String.Format("{0:0.0000}", gyro.frameRotation.y) + "\t" +
            String.Format("{0:0.0000}", gyro.frameRotation.z);
        */
    }
}
