using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUGMotionControlsEnabler : MonoBehaviour
{
    [SerializeField] private float timerOnStartup;

    private BaseControlScheme control;

    public void Start()
    {
        control = GetComponent<PlayerControls>().ControlScheme;
        StartCoroutine(Countdown(timerOnStartup));
    }

    private IEnumerator Countdown(float timer)
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        Debug.Log(control);
        if(control is GyroControlScheme || control is AccelControlScheme)
        {
            control.enabled = false;
            yield return new WaitForSeconds(timer);
            control.enabled = true;
            control.UpdateDefaultRotation();
            Debug.Log("Motion Control Enabled");
        }
    }
}
