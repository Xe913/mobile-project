using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionBasedButton3D : Button3D
{
    [SerializeField] private Interactable interactable;

    protected override void AddToEvents()
    {
        interactable.Add_OnInteraction(TryPressOnInteraction);
        interactable.Add_OnCooldownEnd(ReturnToDefault);
    }

    protected override void RemoveFromEvents()
    {
        if (interactable)
        {
            interactable.Remove_OnInteraction(TryPressOnInteraction);
            interactable.Remove_OnCooldownEnd(ReturnToDefault);
        }
    }

    private void TryPressOnInteraction(Interactable interactable, bool result)
    {
        TryPress(result);
    }

    private void ReturnToDefault(Interactable interactable)
    {
        button.material = defaultMaterial;
    }
}
