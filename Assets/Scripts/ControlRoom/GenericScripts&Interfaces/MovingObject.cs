using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObject : MonoBehaviour
{
    [SerializeField] private int _numberOfLocations = 2;
    [SerializeField] private string NextLocationAnimTrigger = "Next";
    [SerializeField] private string PreviousLocationAnimTrigger = "Previous";

    private Animator animator;
    private int _currentLocation = 1;
    private bool _moving;

    private Action onNextLocationReached;
    private Action onPreviousLocationReached;

    public int CurrentLocation { get => _currentLocation; }
    public int NumberOfLocations { get => _numberOfLocations; }
    public bool Moving { get => _moving; }

    public void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void GoToNextLocation()
    {
        Move(NextLocationAnimTrigger);
    }

    public void GoToPreviousLocation()
    {
        Move(PreviousLocationAnimTrigger);
    }

    private void Move(string animationTrigger)
    {
        _moving = true;
        animator.SetTrigger(animationTrigger);
    }

    private void NextLocationReached()
    {
        _currentLocation = (_currentLocation + 1) % _numberOfLocations;
        Invoke_OnNextLocationReached();
        _moving = false;
    }

    private void PreviousLocationReached()
    {
        _currentLocation = _currentLocation != 0 ? _currentLocation - 1 : _numberOfLocations - 1;
        Invoke_OPreviousLocationReached();
        _moving = false;
    }

    public void Add_OnNextLocationReached(Action listener)
    {
        onNextLocationReached += listener;
    }

    public void Remove_OnNextLocationReached(Action listener)
    {
        if (onNextLocationReached != null) onNextLocationReached -= listener;
    }

    private void Invoke_OnNextLocationReached()
    {
        if (onNextLocationReached != null) onNextLocationReached.Invoke();
    }

    public void Add_OnPreviousLocationReached(Action listener)
    {
        onPreviousLocationReached += listener;
    }

    public void Remove_OnPreviousLocationReached(Action listener)
    {
        if (onPreviousLocationReached != null) onPreviousLocationReached -= listener;
    }

    private void Invoke_OPreviousLocationReached()
    {
        if (onPreviousLocationReached != null) onPreviousLocationReached.Invoke();
    }
}
