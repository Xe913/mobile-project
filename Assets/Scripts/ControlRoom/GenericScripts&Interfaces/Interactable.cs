using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Interactable : MonoBehaviour
{
    [SerializeField] protected float interactCooldown;

    protected ConstDictionary constDict;
    protected GlobalEventsHandler geh;
    protected Player player;

    private bool _canBeInteractedWith;

    private Action<Interactable, bool> OnInteraction; //self, result
    private Action<Interactable> OnCooldownEnd; //self

    public bool CanBeInteractedWith { get => _canBeInteractedWith; }

    public void Start()
    {
        _canBeInteractedWith = true;
        interactCooldown = Mathf.Max(interactCooldown, 0);
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
    }

    private void OnMouseDown()
    {
        Interact();
    }

    protected void Interact()
    {
        //pc check
        if (EventSystem.current.IsPointerOverGameObject()) return;
        //mobile check
        if (Input.touchCount > 0 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId)) return;
        if (player.CanInteract && _canBeInteractedWith)
        {
            bool result = InteractEffect();
            InteractionCooldown();
            Invoke_OnInteraction(result);
            geh.Invoke_OnInteraction(this, result);
        }
    }

    protected virtual bool InteractEffect() {
        return true;
    }

    protected virtual void InteractionCooldown()
    {
        if (interactCooldown > 0) StartCoroutine(StartInteractCooldown());
    }

    protected void EnableInteraction()
    {
        _canBeInteractedWith = true;
        Invoke_OnInteractionEnabled();
    }

    protected void DisableInteraction()
    {
        _canBeInteractedWith = false;
    }

    protected IEnumerator StartInteractCooldown()
    {
        _canBeInteractedWith = false;
        yield return new WaitForSeconds(interactCooldown);
        Invoke_OnInteractionEnabled();
        _canBeInteractedWith = true;
    }

    public void Add_OnInteraction(Action<Interactable, bool> listener)
    {
        OnInteraction += listener;
    }

    public void Remove_OnInteraction(Action<Interactable, bool> listener)
    {
        if (OnInteraction != null) OnInteraction -= listener;
    }

    private void Invoke_OnInteraction(bool result)
    {
        if (OnInteraction != null) OnInteraction.Invoke(this, result);
    }

    public void Add_OnCooldownEnd(Action<Interactable> listener)
    {
        OnCooldownEnd += listener;
    }

    public void Remove_OnCooldownEnd(Action<Interactable> listener)
    {
        if (OnCooldownEnd != null) OnCooldownEnd -= listener;
    }

    private void Invoke_OnInteractionEnabled()
    {
        if (OnCooldownEnd != null) OnCooldownEnd.Invoke(this);
    }
}
