using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Button3D : MonoBehaviour
{
    [SerializeField] protected MeshRenderer button;
    [SerializeField] protected Material pressedMaterial;
    [SerializeField] protected Material pressedWhileInactiveMaterial;

    protected GlobalEventsHandler geh;
    protected Material defaultMaterial;
    protected bool canBePressed = true;

    public void Awake()
    {
        if (button) defaultMaterial = button.material;
    }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        AddToEvents();
    }

    public void OnDestroy()
    {
        RemoveFromEvents();
    }

    protected virtual void TryPress()
    {
        if (canBePressed) Press();
    }

    protected virtual void TryPress(bool success)
    {
        if (canBePressed)
        {
            if (success) Press();
            else FailPress();
        }
    }

    protected virtual void Press()
    {
        if (pressedMaterial) button.material = pressedMaterial;
    }

    protected virtual void FailPress()
    {
        if (pressedWhileInactiveMaterial) button.material = pressedWhileInactiveMaterial;
    }

    protected virtual void Release()
    {
        button.material = defaultMaterial;
    }

    protected virtual void AddToEvents() { }

    protected virtual void RemoveFromEvents() { }
}
