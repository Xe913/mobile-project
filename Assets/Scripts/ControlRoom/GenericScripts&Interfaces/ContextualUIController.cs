using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ContextualUIController : MonoBehaviour
{
    [SerializeField] protected GameObject[] connectedUIElements;
    [SerializeField] protected MovingObject movingObject;
    [SerializeField] protected bool uiActiveWhenDisengaged = false;
    [SerializeField] protected bool objectsActiveWhenDisengaged = false;

    protected ConstDictionary constDict;
    protected GlobalEventsHandler geh;
    protected MeshRenderer[] meshes;
    protected Collider[] colliders;
    protected bool engaged;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        meshes = GetComponentsInChildren<MeshRenderer>();
        colliders = GetComponentsInChildren<Collider>();
        SetUIActive(false);
        SetObjectsActive(objectsActiveWhenDisengaged);
        if (movingObject)
        {
            movingObject.Add_OnNextLocationReached(EngageEnd);
            movingObject.Add_OnPreviousLocationReached(DisengageEnd);
        }
        AddToEvents();
    }

    public void OnDestroy()
    {
        if (movingObject)
        {
            movingObject.Remove_OnNextLocationReached(EngageEnd);
            movingObject.Remove_OnPreviousLocationReached(DisengageEnd);
        }
        RemoveFromEvents();
    }

    protected virtual void AddToEvents() { }

    protected virtual void RemoveFromEvents() { }

    protected virtual void EngageStart()
    {
        if (!engaged)
        {
            engaged = true;
            SetObjectsActive(true);
            if (movingObject) movingObject.GoToNextLocation();
            else EngageEnd();
        }
    }

    protected virtual void EngageEnd()
    {
        SetUIActive(true);
    }

    protected virtual void DisengageStart()
    {
        if (engaged)
        {
            SetUIActive(false);
            if (movingObject) movingObject.GoToPreviousLocation();
            else DisengageEnd();
        }
    }

    protected virtual void DisengageEnd()
    {
        SetObjectsActive(objectsActiveWhenDisengaged);
        engaged = false;
    }

    protected void SetUIActive(bool active)
    {
        foreach (GameObject uiElement in connectedUIElements) uiElement.SetActive(active);
    }

    protected void SetObjectsActive(bool active)
    {
        foreach (MeshRenderer mesh in meshes) mesh.enabled = active;
        foreach (Collider collider in colliders) collider.enabled = active;
    }
}
