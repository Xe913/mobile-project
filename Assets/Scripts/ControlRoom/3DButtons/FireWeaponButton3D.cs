using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireWeaponButton3D : Button3D
{
    protected override void AddToEvents()
    {
        geh.Add_OnStartFiringShipWeapon(Press);
        geh.Add_OnStopFiringShipWeapon(Release);
    }

    protected override void RemoveFromEvents()
    {
        if (geh)
        {
            geh.Remove_OnStartFiringShipWeapon(Press);
            geh.Remove_OnStopFiringShipWeapon(Release);
        }
    }
}
