using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairToolPickUp : Interactable
{
    [SerializeField] private GameObject RepairToolSceneModel;

    public new void Start()
    {
        base.Start();
        geh.Add_OnRepairToolPickUp(OnPickUp);
        geh.Add_OnRepairToolPutDown(OnPutDown);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnRepairToolPickUp(OnPickUp);
            geh.Remove_OnRepairToolPutDown(OnPutDown);
        }
    }

    protected override bool InteractEffect()
    {
        geh.Invoke_OnRepairToolPickUp();
        return true;
    }

    private void OnPickUp()
    {
        DisableInteraction();
        RepairToolSceneModel.SetActive(false);
    }

    private void OnPutDown()
    {
        RepairToolSceneModel.SetActive(true);
        EnableInteraction();
    }
}
