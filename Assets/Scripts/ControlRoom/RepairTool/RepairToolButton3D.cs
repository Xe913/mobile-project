using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairToolButton3D : Button3D
{
    protected override void AddToEvents()
    {
        geh.Add_OnStartFiringRepairBeam(Press);
        geh.Add_OnStopFiringRepairBeam(Release);
    }

    protected override void RemoveFromEvents()
    {
        if (geh)
        {
            geh.Remove_OnStartFiringRepairBeam(Press);
            geh.Remove_OnStopFiringRepairBeam(Release);
        }
    }
}
