using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDamageAlarm : MonoBehaviour
{
    [SerializeField] private Light pulseLight;
    [SerializeField] private float pulseSpeed;
    [SerializeField] private float maxIntensity;
    [Range(0, 1)]
    [SerializeField] private float criticalThreshold = 0.5f;

    private ConstDictionary constDict;
    private HealthSystem shipHS;
    private float shipCriticalThreshold;
    private bool criticalState;

    void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        shipHS = GameObject.FindGameObjectWithTag(constDict.ShipTag).GetComponent<HealthSystem>();
        shipCriticalThreshold = Mathf.Ceil((shipHS.StartingHealth * criticalThreshold));
        shipHS.Add_OnDamageTaken(EnterCriticalState);
        shipHS.Add_OnDamageTaken(ExitCriticalState);
    }

    private void OnDestroy()
    {
        if (shipHS)
        {
            shipHS.Remove_OnDamageTaken(EnterCriticalState);
            shipHS.Remove_OnDamageTaken(ExitCriticalState);
        }
    }

    void Update()
    {
        if (criticalState) pulseLight.intensity = Mathf.PingPong(pulseSpeed * Time.time, 1) * maxIntensity;
    }

    private void EnterCriticalState(int hit, GameObject cause)
    {
        if (hit > 0 && shipHS.CurrentHealth < shipCriticalThreshold) criticalState = true;
    }

    private void ExitCriticalState(int hit, GameObject cause)
    {
        if (hit < 0 && shipHS.CurrentHealth >= shipCriticalThreshold)
        {
            pulseLight.intensity = 0;
            criticalState = false;
        }
    }
}
