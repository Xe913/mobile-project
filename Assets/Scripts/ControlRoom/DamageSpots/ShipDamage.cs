using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipDamage : MonoBehaviour
{
    [SerializeField] private int damageEntity;

    private DamageSpotsController controller;
    private int _index;
    private int currentEntity;

    public int Index { get => _index; }

    public void Awake()
    {
        currentEntity = damageEntity;
    }

    public void AssignControllerData(DamageSpotsController controller, int index)
    {
        this.controller = controller;
        _index = index;
    }

    public void ReduceDamage(int reducedBy)
    {
        currentEntity -= reducedBy;
        Debug.Log("reduced to " + currentEntity);
        if (currentEntity <= 0) RemoveShipDamage();
    }

    public void RemoveShipDamage()
    {
        controller.RepairDamage(this);
        Destroy(gameObject);
    }
}
