using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageSpotsController : MonoBehaviour
{
    [SerializeField] private GameObject damageEffect;
    [SerializeField] private List<Transform> damageSpots;
    [Range(0, 1)]
    [SerializeField] private float criticalThreshold = 0.5f;
    [SerializeField] private int maxRandomSpawnTries = 20;

    private ConstDictionary constDict;
    private List<GameObject> instancedEffects;
    private int activeDamageSpots = 0;
    private HealthSystem shipHS;
    private float shipCriticalThreshold;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        instancedEffects = new List<GameObject>();
        foreach(Transform spot in damageSpots) instancedEffects.Add(null);
        shipHS = GameObject.FindGameObjectWithTag(constDict.ShipTag).GetComponent<HealthSystem>();
        shipCriticalThreshold = Mathf.Ceil((shipHS.StartingHealth * criticalThreshold));
        shipHS.Add_OnDamageTaken(TrySpawnDamageSpot);
    }

    public void OnDestroy()
    {
        if (shipHS) shipHS.Remove_OnDamageTaken(TrySpawnDamageSpot);
    }

    private void TrySpawnDamageSpot(int damage, GameObject cause)
    {
        if (activeDamageSpots < damageSpots.Count && damage > 0 && shipHS.CurrentHealth < shipCriticalThreshold)
            CalcDamageSpotSpawn();
    }

    private void CalcDamageSpotSpawn()
    {
        int chosenSpotIndex = -1;
        bool spawnOk = false;
        for (int i = 0; i < maxRandomSpawnTries; i++)
        {
            chosenSpotIndex = Random.Range(0, damageSpots.Count);
            spawnOk = instancedEffects[chosenSpotIndex] == null;
            if (spawnOk) break;
        }
        if (!spawnOk)
        {
            for (int i = 0; i < damageSpots.Count; i++)
            {
                if (instancedEffects[i] == null)
                {
                    chosenSpotIndex = i;
                    spawnOk = true;
                    break;
                }
            }
        }
        if (spawnOk) SpawnDamageSpot(chosenSpotIndex);
    }

    private void SpawnDamageSpot(int index)
    {

        activeDamageSpots++;
        instancedEffects[index] = Instantiate(damageEffect, damageSpots[index]);
        instancedEffects[index].GetComponent<ShipDamage>().AssignControllerData(this, index);
    }

    public void RepairDamage(ShipDamage damage)
    {

        int repaired = Mathf.CeilToInt((shipHS.StartingHealth - shipHS.CurrentHealth) / activeDamageSpots);
        activeDamageSpots = Mathf.Max(activeDamageSpots - 1, 0);
        instancedEffects[damage.Index] = null;
        shipHS.ProcessHit(-repaired, gameObject);
    }
}
