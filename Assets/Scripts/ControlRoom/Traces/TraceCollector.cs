using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraceCollector : MonoBehaviour
{
    [SerializeField] private int maxTraces = 5;
    [SerializeField] private Transform container;
    [SerializeField] private Transform spawnPoint;
    [SerializeField] private string AnimationTrigger = "Consume";
    [SerializeField] private AudioClip consumeSound;

    private GlobalEventsHandler geh;
    private SoundEffectController sfxController;
    private Animator animator;
    private List<GameObject> collectedTraces;

    private Action onTraceCollected;
    private Action<int> onTracesConsumed;
    private bool _animationRunning;

    public int CollectedTracesCount { get => collectedTraces.Count; }
    public bool AnimationRunning { get => _animationRunning; }

    void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
        animator = GetComponent<Animator>();
        if (spawnPoint == null) spawnPoint = container;
        collectedTraces = new List<GameObject>();
        geh.Add_OnLootObtained(CollectTrace);
    }

    public void OnDestroy()
    {
        if (geh) geh.Remove_OnLootObtained(CollectTrace);
    }

    private void CollectTrace(Loot loot)
    {
        if(CollectedTracesCount < maxTraces && loot is Trace)
        {
            GameObject collectedTrace = Instantiate(loot.CollectedModel, spawnPoint.position, UnityEngine.Random.rotation);
            collectedTrace.transform.SetParent(container);
            collectedTraces.Add(collectedTrace);
            Invoke_OnTraceCollected();
        }
    }

    public void ConsumeAllTracesRequest()
    {
        ConsumeTracesRequest(collectedTraces.Count);
    }

    private int tracesToRemove;
    public void ConsumeTracesRequest(int numberToRemove)
    {
        if (!_animationRunning)
        {
            _animationRunning = true;
            tracesToRemove = numberToRemove;
            sfxController.PlaySoundEffect(consumeSound);
            StartConsumeAnimation();
        }
    }

    private void StartConsumeAnimation()
    {
        animator.SetTrigger(AnimationTrigger);
    }

    private void ConsumeTraces()
    {
        int removeAt;
        int leftToRemove = tracesToRemove;
        while (leftToRemove > 0 && collectedTraces.Count > 0)
        {
            removeAt = UnityEngine.Random.Range(0, collectedTraces.Count);
            Destroy(collectedTraces[removeAt]);
            collectedTraces.RemoveAt(removeAt);
            leftToRemove--;
        }
        Invoke_OnTracesConsumed(tracesToRemove);
        tracesToRemove = 0;
    }

    private void EndConsumeAnimation()
    {
        _animationRunning = false;
    }

    public void Add_OnTraceCollected(Action listener)
    {
        onTraceCollected += listener;
    }

    public void Remove_OnTraceCollected(Action listener)
    {
        onTraceCollected -= listener;
    }

    private void Invoke_OnTraceCollected()
    {
        if (onTraceCollected != null) onTraceCollected.Invoke();
    }


    public void Add_OnTracesConsumed(Action<int> listener)
    {
        onTracesConsumed += listener;
    }

    public void Remove_OnTracesConsumed(Action<int> listener)
    {
        onTracesConsumed -= listener;
    }

    private void Invoke_OnTracesConsumed(int consumed)
    {
        if (onTracesConsumed != null) onTracesConsumed.Invoke(consumed);
    }
}
