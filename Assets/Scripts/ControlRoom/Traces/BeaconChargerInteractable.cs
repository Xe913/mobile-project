using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconChargerInteractable : Interactable
{
    [SerializeField] private int tracesPerUse = 3;
    [SerializeField] private int chargesPerUse = 1;
    [SerializeField] private Beacon beacon;
    [SerializeField] private TraceCollector traceCollector;

    public new void Start()
    {
        base.Start();
        traceCollector.Add_OnTracesConsumed(AddCharges);
    }

    public void OnDestroy()
    {
        if (traceCollector)
        {
            traceCollector.Remove_OnTracesConsumed(AddCharges);
        }
    }

    protected override bool InteractEffect()
    {
        if (CanRecharge())
        {
            traceCollector.ConsumeTracesRequest(tracesPerUse);
            return true;
        }
        return false;
            
    }

    private void AddCharges(int consumedTraces)
    {
        beacon.AddCharges(chargesPerUse);
    }

    private bool CanRecharge()
    {
        return !traceCollector.AnimationRunning
            && traceCollector.CollectedTracesCount >= tracesPerUse
            && beacon.CurrentCharges < beacon.MaxCharges;
    }
}
