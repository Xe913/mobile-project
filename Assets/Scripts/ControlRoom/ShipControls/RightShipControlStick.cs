using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightShipControlStick : MonoBehaviour
{
    [SerializeField] private float movementRange = 1;

    protected GlobalEventsHandler geh;
    Vector3 startingPosition;
    Vector3 movePosition;

    private void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        geh.Add_OnShipMovementInput(UpdateStickPosition);
        startingPosition = transform.localPosition;
        movePosition = Vector2.zero;
    }

    public void OnDestroy()
    {
        geh.Remove_OnShipMovementInput(UpdateStickPosition);
    }

    private void UpdateStickPosition(float position)
    {
        movePosition.y = position * movementRange;
        transform.localPosition = startingPosition + movePosition;
    }
}
