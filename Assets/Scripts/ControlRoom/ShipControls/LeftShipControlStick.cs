using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftShipControlStick : MonoBehaviour
{
    [SerializeField] private Vector2 movementRange = Vector2.one;

    protected GlobalEventsHandler geh;
    Vector3 startingPosition;
    Vector3 movePosition;

    private void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        geh.Add_OnShipRotationInput(UpdateStickPosition);
        startingPosition = transform.localPosition;
        movePosition = Vector2.zero;
    }

    public void OnDestroy()
    {
        geh.Remove_OnShipRotationInput(UpdateStickPosition);
    }

    private void UpdateStickPosition(Vector2 position)
    {
        movePosition.x = position.x * movementRange.x;
        movePosition.y = position.y * movementRange.y;
        transform.localPosition = startingPosition + movePosition;
    }
}
