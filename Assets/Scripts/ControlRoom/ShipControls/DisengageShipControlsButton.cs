using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisengageShipControlsButton : MonoBehaviour
{
    private GlobalEventsHandler geh;
    private Player player;
    private PlayerControls pc;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
        pc = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<PlayerControls>();
    }

    public void DisengageButton()
    {
        if (player.CanInteract)
        {
            player.CanInteract = false;
            pc.EnableMovement();
            geh.Invoke_OnRequestControlsDisengage();
        }
    }
}
