using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlsOpenerInteractable : Interactable
{
    public new void Start()
    {
        base.Start();
        geh.Add_OnRequestShipControlsDisengage(EnableOnDisengage);
    }

    public void OnDestroy()
    {
        if (geh) geh.Remove_OnRequestShipControlsDisengage(EnableOnDisengage);
    }

    protected override bool InteractEffect()
    {
        player.CanInteract = false;
        DisableInteraction();
        geh.Invoke_OnRepairToolPutDown();
        geh.Invoke_OnRequestShipControlsEngage();
        return true;
    }

    private void EnableOnDisengage()
    {
        EnableInteraction();
    }
}
