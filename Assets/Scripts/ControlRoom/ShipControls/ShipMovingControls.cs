using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovingControls : MonoBehaviour
{
    [SerializeField] private GameObject connectedUIElement;
    [SerializeField] private Transform engagedRotation;
    [SerializeField] private Transform disengagedRotation;
    [SerializeField] private float rotationSpeed;
    [SerializeField] private bool invisibleWhenDisengaged = true;

    private ConstDictionary constDict;
    private GlobalEventsHandler geh;
    private GameObject uiElement;
    private bool engaged = false;
    private Action<ShipMovingControls> OnComponentInPosition;


    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalEventsHandler>();
        if (connectedUIElement)
        {
            uiElement = Instantiate(connectedUIElement, GameObject.FindGameObjectWithTag(constDict.ShipControlsCanvas).transform);
            uiElement.SetActive(engaged);
        }
        EnableMesh(!invisibleWhenDisengaged);
        geh.Add_OnRequestShipControlsEngage(EngageControls);
        geh.Add_OnRequestShipControlsDisengage(DisengageControls);
    }

    private void OnDestroy()
    {
        geh.Remove_OnRequestShipControlsEngage(EngageControls);
        geh.Remove_OnRequestShipControlsDisengage(DisengageControls);
    }

    private void EngageControls()
    {
        engaged = true;
        //anim.SetTrigger("Engage");
        StartCoroutine(Rotate(engagedRotation.rotation, invisibleWhenDisengaged));
    }

    private void DisengageControls()
    {
        engaged = false;
        if (uiElement) uiElement.SetActive(false);
        //anim.SetTrigger("Disengage");
        StartCoroutine(Rotate(disengagedRotation.rotation, invisibleWhenDisengaged));
        //csc.CanInteract = true;
    }

    private IEnumerator Rotate(Quaternion targetRotation, bool makeInvisible)
    {
        if (engaged && makeInvisible) EnableMesh(engaged);
        while (transform.rotation != targetRotation)
        {
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            yield return null;
        }
        if (engaged)
        {
            if (uiElement) uiElement.SetActive(true);
        }
        else
        {
            if (makeInvisible) EnableMesh(false);
        }
        if (OnComponentInPosition != null) OnComponentInPosition.Invoke(this);
    }

    private void EnableMesh(bool enable)
    {
        MeshRenderer[] mrs = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in mrs) mr.enabled = enable;
    }

    public void Add_OnComponentInPosition(Action<ShipMovingControls> listener)
    {
        OnComponentInPosition += listener;
    }
    public void Remove_OnComponentInPosition(Action<ShipMovingControls> listener)
    {
        OnComponentInPosition -= listener;
    }
}
