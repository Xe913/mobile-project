using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlsOpenerUIController : ShipUIController
{
    [SerializeField] private MeshRenderer disengageText;

    protected override void EngageStart()
    {
        base.EngageStart();
        disengageText.enabled = false;
    }

    protected override void EngageEnd()
    {
        base.EngageEnd();
        disengageText.enabled = true;
    }

    protected override void DisengageStart()
    {
        disengageText.enabled = false;
        base.DisengageStart();
    }

    protected override void DisengageEnd()
    {
        base.DisengageEnd();
        disengageText.enabled = false;
    }
}
