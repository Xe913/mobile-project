using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchViewController : MonoBehaviour
{
    [SerializeField] private GameObject screen;
    [SerializeField] private List<GameObject> viewButtons;

    private MeshRenderer screenMesh;
    private List<SwitchViewButton> buttons;

    void Start()
    {
        screenMesh = screen.GetComponent<MeshRenderer>();
        buttons = new List<SwitchViewButton>();
        foreach (GameObject viewButtonGO in viewButtons)
        {
            SwitchViewButton switchViewButton = viewButtonGO.GetComponent<SwitchViewButton>();
            if (switchViewButton)
            {
                buttons.Add(switchViewButton);
                switchViewButton.Add_OnSwitchView(SwitchView);
            }
        }
        SwitchView(buttons[0]);
    }

    public void OnDestroy()
    {
        foreach(SwitchViewButton button in buttons)
        {
            if (button) button.Remove_OnSwitchView(SwitchView);
        }
    }

    private void SwitchView(SwitchViewButton activeView)
    {
        foreach (SwitchViewButton button in buttons) button.SetAsDefaultButton();
        activeView.SetAsActiveButton();
        screenMesh.material = activeView.ViewMaterial;
    }
}
