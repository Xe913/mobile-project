using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreezeShipInteractable : Interactable
{
    protected override bool InteractEffect()
    {
        geh.Invoke_OnFreezeShip();
        return true;
    }
}
