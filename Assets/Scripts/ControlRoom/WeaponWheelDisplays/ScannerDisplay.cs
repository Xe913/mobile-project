using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScannerDisplay : MonoBehaviour
{
    [SerializeField] private Scanner scanner;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color readyColor;
    [Range(0, 10)]
    [SerializeField] private int bars = 3;
    [SerializeField] private float cyclingSpeed;

    private TextMesh tm;
    private WaitForSeconds waitCycle;
    private int lastBar;

    public void Start()
    {
        tm = GetComponent<TextMesh>();
        waitCycle = new WaitForSeconds(cyclingSpeed);
        StartCoroutine(Cycle());
    }

    private bool previousState = false;
    private bool currentState = true;
    public void Update()
    {
        currentState = scanner.CanFire();
        UpdateText();
        previousState = currentState;
    }

    public void UpdateText()
    {
        if (!currentState)
        {
            if (previousState != currentState) lastBar = 0;
            tm.text = "CHARGING\n";
            for(int i = 0; i < bars; i++)
            {
                tm.text += " ";
                if (i < lastBar) tm.text += "_";
                else tm.text += " ";
            }
            tm.text += " ";
            tm.color = defaultColor;
        }
        else
        {
            tm.text = "SCANNER\nREADY";
            tm.color = readyColor;
        }
    }

    public IEnumerator Cycle()
    {
        while (true)
        {
            yield return waitCycle;
            lastBar = (lastBar + 1) % (bars + 1);
        }
    }
}
