using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetDisplay : MonoBehaviour
{
    [SerializeField] private Magnet magnet;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color readyColor;

    private TextMesh tm;

    public void Start()
    {
        tm = GetComponent<TextMesh>();
    }

    private bool lastFrameFire = false;
    private bool lastFrameReturning = false;
    public void Update()
    {
        if (magnet.CanFire() != lastFrameFire || magnet.Returning != lastFrameReturning) UpdateText();
        lastFrameFire = magnet.CanFire();
        lastFrameReturning = magnet.Returning;
    }

    public void UpdateText()
    {
        if (magnet.CanFire())
        {
            tm.text = "READY";
            tm.color = readyColor;
        }
        else
        {
            tm.color = defaultColor;
            if (!magnet.Returning) tm.text = "SHOT";
            else tm.text = "RETURNING";
        }
    }
}
