using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconDisplay : MonoBehaviour
{
    [SerializeField] private Beacon beacon;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color readyColor;
    [SerializeField] private float cyclingSpeed;

    private TextMesh tm;

    public void Start()
    {
        tm = GetComponent<TextMesh>();
    }

    private bool lastFrameCanFire;
    private int lastFrameCharges = -1;
    public void Update()
    {
        if(beacon.CanFire() != lastFrameCanFire || beacon.CurrentCharges != lastFrameCharges)
        UpdateText();
        lastFrameCanFire = beacon.CanFire();
        lastFrameCharges = beacon.CurrentCharges;
    }

    private int chargesToDisplay;
    public void UpdateText()
    {
        tm.text = "BEACONS\n";
        if (!beacon.CanFire()) tm.color = defaultColor;
        else tm.color = readyColor;
        chargesToDisplay = beacon.CurrentCharges;
        for(int i = 0; i < beacon.MaxCharges; i++)
        {
            if (chargesToDisplay > 0)
            {
                tm.text += " O";
                chargesToDisplay--;
            }
            else
            {
                tm.text += " -";
            }
            tm.text += " ";
        }
    }
}
