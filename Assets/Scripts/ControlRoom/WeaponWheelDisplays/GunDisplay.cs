using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunDisplay : MonoBehaviour
{
    [SerializeField] private Gun gun;
    [SerializeField] private Color defaultColor;
    [SerializeField] private Color halfHeatColor;
    [SerializeField] private Color overheatColor;
    [Range(0, 10)]
    [SerializeField] private int bars = 10;

    private TextMesh tm;
    private int currentBars;
    private int previousBars;

    void Start()
    {
        tm = GetComponent<TextMesh>();
        previousBars = -1;
    }

    void Update()
    {
        currentBars = Mathf.FloorToInt(gun.CurrentHeatPercentage() * 100 / bars);
        if (currentBars != previousBars) UpdateText(currentBars);
        previousBars = currentBars;
    }

    private void UpdateText(int barsToDisplay)
    {
        tm.text = "HEAT LEVEL\n";
        if (barsToDisplay < bars)
        {
            if (barsToDisplay <= bars / 2) tm.color = defaultColor;
            else tm.color = halfHeatColor;
            for (int i = 0; i < bars; i++)
            {
                if (barsToDisplay > 0)
                {
                    tm.text += "|";
                    barsToDisplay--;
                }
                else
                {
                    tm.text += " ";
                }
            }
        }
        else
        {
            tm.text += "OVERHEAT";
            tm.color = overheatColor;
        }
    }
}
