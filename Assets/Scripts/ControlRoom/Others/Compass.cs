using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass : MonoBehaviour
{
    [SerializeField] private Transform movingCompass;
    [SerializeField] private Transform followRotation;

    public void Update()
    {
        movingCompass.localRotation = followRotation.rotation;
    }
}
