using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEventsHandler : MonoBehaviour
{
    private static GlobalEventsHandler _instance;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    //touch
    private Action<Vector3> _onTouchStart;
    private Action<Vector3> _onTouchEnd;

    //interaction with scene objects
    private Action<Interactable, bool> _onInteraction; //interactable, result
    private Action _onStartFiringRepairBeam;
    private Action _onStopFiringRepairBeam;
    private Action _onRepairToolPickUp;
    private Action _onRepairToolPutDownRequest;
    private Action _onRepairToolPutDownComplete;
    private Action _onFreezeShip;

    //ship controls
    private Action _onRequestShipControlsEngage;
    private Action _onRequestShipControlsDisengage;
    private Action _onShipControlsEngaged;

    //ship events
    private Action<float> _onShipMovementInput;
    private Action<Vector2> _onShipRotationInput;
    private Action _onStartFiringShipWeapon;
    private Action _onStopFiringShipWeapon;
    private Action<SwapDirection> _onWeaponSwapInput;
    private Action<Weapon> _onWeaponSwapped;

    //scene events
    private Action<Asteroid> _onAsteroidSpawn;
    private Action<Asteroid, GameObject> _onAsteroidDestruction; //asteroid, cause
    private Action<Loot> _onLootObtained;
    private Action<GameObject> _onTreasureSpawn; //treasure, or object which contains one in its loot table
    private Action<GameObject> _onTreasureRemoved; //treasure, or object which contains one in its loot table

    //game flow
    private Action _onWinConditionAchieved;
    private Action _onLoseConditionAchieved;
    private Action _onGameWon;
    private Action _onGameLost;

    //add
    public void Add_OnTouchStart(Action<Vector3> listener) { _onTouchStart += listener; }
    public void Add_OnTouchEnd(Action<Vector3> listener) { _onTouchEnd += listener; }

    public void Add_OnInteraction(Action<Interactable, bool> listener) { _onInteraction += listener; }
    public void Add_OnStartFiringRepairBeam(Action listener) { _onStartFiringRepairBeam += listener; }
    public void Add_OnStopFiringRepairBeam(Action listener) { _onStopFiringRepairBeam += listener; }
    public void Add_OnRepairToolPickUp(Action listener) { _onRepairToolPickUp += listener; }
    public void Add_OnRepairToolPutDown(Action listener) { _onRepairToolPutDownRequest += listener; }
    public void Add_OnFreezeShip(Action listener) { _onFreezeShip += listener; }

    public void Add_OnRequestShipControlsEngage(Action listener) { _onRequestShipControlsEngage += listener; }
    public void Add_OnRequestShipControlsDisengage(Action listener) { _onRequestShipControlsDisengage += listener; }
    public void Add_OnShipControlsEngaged(Action listener) { _onShipControlsEngaged += listener; }

    public void Add_OnShipMovementInput(Action<float> listener) { _onShipMovementInput += listener; }
    public void Add_OnShipRotationInput(Action<Vector2> listener) { _onShipRotationInput += listener; }
    public void Add_OnStartFiringShipWeapon(Action listener) { _onStartFiringShipWeapon += listener; }
    public void Add_OnStopFiringShipWeapon(Action listener) { _onStopFiringShipWeapon += listener; }
    public void Add_OnWeaponSwapInput(Action<SwapDirection> listener) { _onWeaponSwapInput += listener; }
    public void Add_OnWeaponSwapped(Action<Weapon> listener) { _onWeaponSwapped += listener; }

    public void Add_OnAsteroidSpawn(Action<Asteroid> listener) { _onAsteroidSpawn += listener; }
    public void Add_OnAsteroidDestruction(Action<Asteroid, GameObject> listener) { _onAsteroidDestruction += listener; }
    public void Add_OnLootObtained(Action<Loot> listener) { _onLootObtained += listener; }
    public void Add_OnTreasureSpawn(Action<GameObject> listener) { _onTreasureSpawn += listener; }
    public void Add_OnTreasureRemoved(Action<GameObject> listener) { _onTreasureRemoved += listener; }

    public void Add_OnWinConditionAchieved(Action listener) { _onWinConditionAchieved += listener; }
    public void Add_OnLoseConditionAchieved(Action listener) { _onLoseConditionAchieved += listener; }
    public void Add_OnGameWon(Action listener) { _onGameWon += listener; }
    public void Add_OnGameLost(Action listener) { _onGameLost += listener; }

    //remove
    public void Remove_OnTouchStart(Action<Vector3> listener) { if (_onTouchStart != null) _onTouchStart -= listener; }
    public void Remove_OnTouchEnd(Action<Vector3> listener) { if (_onTouchEnd != null) _onTouchEnd -= listener; }

    public void Remove_OnInteraction(Action<Interactable, bool> listener) { if (_onInteraction != null) _onInteraction -= listener; }
    public void Remove_OnStartFiringRepairBeam(Action listener) { if (_onStartFiringRepairBeam != null) _onStartFiringRepairBeam -= listener; }
    public void Remove_OnStopFiringRepairBeam(Action listener) { if (_onStopFiringRepairBeam != null) _onStopFiringRepairBeam -= listener; }
    public void Remove_OnRepairToolPickUp(Action listener) { if (_onRepairToolPickUp != null) _onRepairToolPickUp -= listener; }
    public void Remove_OnRepairToolPutDown(Action listener) { if (_onRepairToolPutDownRequest != null) _onRepairToolPutDownRequest -= listener; }
    public void Remove_OnFreezeShip(Action listener) { if (_onFreezeShip != null) _onFreezeShip -= listener; }

    public void Remove_OnRequestShipControlsEngage(Action listener) { if (_onRequestShipControlsEngage != null) _onRequestShipControlsEngage -= listener; }
    public void Remove_OnRequestShipControlsDisengage(Action listener) { if (_onRequestShipControlsDisengage != null) _onRequestShipControlsDisengage -= listener; }
    public void Remove_OnShipControlsEngaged(Action listener) { if (_onShipControlsEngaged != null) _onShipControlsEngaged += listener; }

    public void Remove_OnShipMovementInput(Action<float> listener) { if (_onShipMovementInput != null) _onShipMovementInput -= listener; }
    public void Remove_OnShipRotationInput(Action<Vector2> listener) { if(_onShipRotationInput != null) _onShipRotationInput -= listener; }
    public void Remove_OnStartFiringShipWeapon(Action listener) { if (_onStartFiringShipWeapon != null) _onStartFiringShipWeapon -= listener; }
    public void Remove_OnStopFiringShipWeapon(Action listener) { if (_onStopFiringShipWeapon != null) _onStopFiringShipWeapon -= listener; }
    public void Remove_OnWeaponSwapInput(Action<SwapDirection> listener) { if (_onWeaponSwapInput != null) _onWeaponSwapInput -= listener; }
    public void Remove_OnWeaponSwapped(Action<Weapon> listener) { if (_onWeaponSwapped != null) _onWeaponSwapped -= listener; }

    public void Remove_OnAsteroidSpawn(Action<Asteroid> listener) { if (_onAsteroidSpawn != null) _onAsteroidSpawn -= listener; }
    public void Remove_OnAsteroidDestruction(Action<Asteroid, GameObject> listener) { if (_onAsteroidDestruction != null) _onAsteroidDestruction -= listener; }
    public void Remove_OnLootObtained(Action<Loot> listener) { if (_onLootObtained != null) _onLootObtained -= listener; }
    public void Remove_OnTreasureSpawn(Action<GameObject> listener) { if (_onTreasureSpawn != null) _onTreasureSpawn -= listener; }
    public void Remove_OnTreasureRemoved(Action<GameObject> listener) { if (_onTreasureRemoved != null) _onTreasureRemoved -= listener; }

    public void Remove_OnWinConditionAchieved(Action listener) { if (_onWinConditionAchieved != null) _onWinConditionAchieved -= listener; }
    public void Remove_OnLoseConditionAchieved(Action listener) { if (_onLoseConditionAchieved != null) _onLoseConditionAchieved -= listener; }
    public void Remove_OnGameWon(Action listener) { if (_onGameWon != null) _onGameWon -= listener; }
    public void Remove_OnGameLost(Action listener) { if (_onGameLost != null) _onGameLost -= listener; }


    //invoke
    public void Invoke_OnTouchStart(Vector3 touchPoint) { if (_onTouchStart != null) _onTouchStart.Invoke(touchPoint); }
    public void Invoke_OnTouchEnd(Vector3 touchPoint) { if (_onTouchEnd != null) _onTouchEnd.Invoke(touchPoint); }
    public void Invoke_OnInteraction(Interactable interactable, bool result) { if (_onInteraction != null) _onInteraction.Invoke(interactable, result); }
    public void Invoke_OnStartFiringRepairBeam() { if (_onStartFiringRepairBeam != null) _onStartFiringRepairBeam.Invoke(); }
    public void Invoke_OnStopFiringRepairBeam() { if (_onStopFiringRepairBeam != null) _onStopFiringRepairBeam.Invoke(); }
    public void Invoke_OnRepairToolPickUp() { if (_onRepairToolPickUp != null) _onRepairToolPickUp.Invoke(); }
    public void Invoke_OnRepairToolPutDown() { if (_onRepairToolPutDownRequest != null) _onRepairToolPutDownRequest.Invoke(); }
    public void Invoke_OnFreezeShip() { if (_onFreezeShip != null) _onFreezeShip.Invoke(); }

    public void Invoke_OnRequestShipControlsEngage() { if (_onRequestShipControlsEngage != null) _onRequestShipControlsEngage.Invoke(); }
    public void Invoke_OnRequestControlsDisengage() { if (_onRequestShipControlsDisengage != null) _onRequestShipControlsDisengage.Invoke(); }
    public void Invoke_OnShipControlsEngaged() { if (_onShipControlsEngaged != null) _onShipControlsEngaged.Invoke(); }

    public void Invoke_OnShipMovementInput(float movementInput) { if (_onShipMovementInput != null) _onShipMovementInput.Invoke(movementInput); }
    public void Invoke_OnShipRotationInput(Vector2 rotationInput) { if (_onShipRotationInput != null) _onShipRotationInput.Invoke(rotationInput); }
    public void Invoke_OnStartFiringShipWeapon() { if (_onStartFiringShipWeapon != null) _onStartFiringShipWeapon.Invoke(); }
    public void Invoke_OnStopFiringShipWeapon() { if (_onStopFiringShipWeapon != null) _onStopFiringShipWeapon.Invoke(); }
    public void Invoke_OnWeaponSwapInput(SwapDirection direction) { if (_onWeaponSwapInput != null) _onWeaponSwapInput.Invoke(direction); }
    public void Invoke_OnWeaponSwapped(Weapon activeWeapon) { if (_onWeaponSwapped != null) _onWeaponSwapped.Invoke(activeWeapon); }

    public void Invoke_OnAsteroidSpawn(Asteroid asteroid) { if (_onAsteroidSpawn != null) _onAsteroidSpawn.Invoke(asteroid); }
    public void Invoke_OnAsteroidDestruction(Asteroid asteroid, GameObject cause) { if (_onAsteroidDestruction != null) _onAsteroidDestruction.Invoke(asteroid, cause); }
    public void Invoke_OnLootObtained(Loot loot) { if (_onLootObtained != null) _onLootObtained.Invoke(loot); }
    public void Invoke_OnTreasureSpawn(GameObject treasureRelatedObject) { if (_onTreasureSpawn != null) _onTreasureSpawn.Invoke(treasureRelatedObject); }
    public void Invoke_OnTreasureRemoved(GameObject treasureRelatedObject) { if (_onTreasureRemoved != null) _onTreasureRemoved.Invoke(treasureRelatedObject); }

    public void Invoke_OnWinConditionAchieved() { if (_onWinConditionAchieved != null) _onWinConditionAchieved.Invoke(); }
    public void Invoke_OnLoseConditionAchieved() { if (_onLoseConditionAchieved != null) _onLoseConditionAchieved.Invoke(); }
    public void Invoke_OnGameWon() { if (_onGameWon != null) _onGameWon.Invoke(); }
    public void Invoke_OnGameLost() { if (_onGameLost != null) _onGameLost.Invoke(); }
}
