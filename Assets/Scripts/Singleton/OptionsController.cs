using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DifficultySettings { normal, hard, Count }

public class OptionsController : MonoBehaviour
{
    private ConstDictionary constDict;
    private SaveManager sm;
    private DifficultySettings _difficulty;
    [Header("DEBUG")]
    [SerializeField] private float _bgMusicVolume;
    [SerializeField] private float _soundEffectsVolume;

    private Action OnBgMusicVolumeChange;

    public DifficultySettings Difficulty { get => _difficulty; }
    public float BgMusicVolume { get => _bgMusicVolume; }
    public float SoundEffectsVolume { get => _soundEffectsVolume; }

    private static OptionsController _instance;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sm = GameObject.FindGameObjectWithTag("GameController").GetComponent<SaveManager>();
        LoadDifficultySettings();
        LoadBgMusicVolumeSettings();
        LoadSoundEffectsVolumeSettings();
        sm.Add_OnClearData(UpdateOptions);
    }

    private void OnDestroy()
    {
        if (sm) sm.Remove_OnClearData(UpdateOptions);
    }

    public void ClearOptionsData()
    {
        PlayerPrefs.DeleteKey(constDict.DifficultySettingsKey);
        PlayerPrefs.DeleteKey(constDict.BgMusicVolumeSettingsKey);
        PlayerPrefs.DeleteKey(constDict.SoundEffectsVolumeSettingsKey);
        UpdateOptions();
    }

    //LOAD
    private void LoadDifficultySettings()
    {
        if (!PlayerPrefs.HasKey(constDict.DifficultySettingsKey))
            PlayerPrefs.SetInt(constDict.DifficultySettingsKey, (int)constDict.DefaultDifficultySetting);
        _difficulty = (DifficultySettings)PlayerPrefs.GetInt(constDict.DifficultySettingsKey);
    }

    private void LoadBgMusicVolumeSettings()
    {
        if (!PlayerPrefs.HasKey(constDict.BgMusicVolumeSettingsKey))
            PlayerPrefs.SetFloat(constDict.BgMusicVolumeSettingsKey, constDict.DefaultBgMusicVolume);
        _bgMusicVolume = PlayerPrefs.GetFloat(constDict.BgMusicVolumeSettingsKey);
    }

    private void LoadSoundEffectsVolumeSettings()
    {
        if (!PlayerPrefs.HasKey(constDict.SoundEffectsVolumeSettingsKey))
            PlayerPrefs.SetFloat(constDict.SoundEffectsVolumeSettingsKey, constDict.DefaultSoundEffectsVolume);
        _soundEffectsVolume = PlayerPrefs.GetFloat(constDict.SoundEffectsVolumeSettingsKey);
    }

    //SAVE

    private void SaveDifficultySettings()
    {
        PlayerPrefs.SetInt(constDict.DifficultySettingsKey, (int)_difficulty);
    }

    private void SaveBgMusicVolumeSettings()
    {
        PlayerPrefs.SetFloat(constDict.BgMusicVolumeSettingsKey, _bgMusicVolume);
    }

    private void SaveSoundEffectsVolumeSettings()
    {
        PlayerPrefs.SetFloat(constDict.SoundEffectsVolumeSettingsKey, _soundEffectsVolume);
    }

    //UPDATE

    private void UpdateOptions()
    {
        LoadDifficultySettings();
        LoadBgMusicVolumeSettings();
        LoadSoundEffectsVolumeSettings();
        UpdateDifficulty(_difficulty);
        UpdateBgMusicVolume(_bgMusicVolume);
        UpdateSoundEffectsVolume(_soundEffectsVolume);
    }

    public void UpdateDifficulty(DifficultySettings newDifficulty)
    {
        _difficulty = newDifficulty;
        SaveDifficultySettings();
    }
    
    public void UpdateBgMusicVolume(float newVolume)
    {
        _bgMusicVolume = Mathf.Clamp(0, newVolume, 1);
        SaveBgMusicVolumeSettings();
        Invoke_OnBgMusicVolumeChange();
    }

    public void UpdateSoundEffectsVolume(float newVolume)
    {
        _soundEffectsVolume = Mathf.Clamp(0, newVolume, 1);
        SaveSoundEffectsVolumeSettings();
    }

    //EVENTS
    public void Add_OnBgMusicVolumeChange(Action listener)
    {
        OnBgMusicVolumeChange += listener;
    }

    public void Remove_OnBgMusicVolumeChange(Action listener)
    {
        if (OnBgMusicVolumeChange != null) OnBgMusicVolumeChange -= listener;
    }

    private void Invoke_OnBgMusicVolumeChange()
    {
        if (OnBgMusicVolumeChange != null) OnBgMusicVolumeChange.Invoke();
    }
}
