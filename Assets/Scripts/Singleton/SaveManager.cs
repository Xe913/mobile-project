using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveManager : MonoBehaviour
{
    private ConstDictionary constDict;

    private Action OnClearData;

    private static SaveManager _instance;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
    }

    private string LevelNumberToName(int levelNumber)
    {
        return constDict.LevelScenePrefix + levelNumber.ToString();
    }

    public void LevelWon()
    {
        SaveLevelData(SceneManager.GetActiveScene().name);
    }

    public void SaveLevelData(int levelNumber)
    {
        SaveLevelData(LevelNumberToName(levelNumber));
    }

    public void SaveLevelData(string levelName)
    {
        PlayerPrefs.SetInt(levelName, (int)Time.timeSinceLevelLoad);
    }

    public int LoadLevelData(int levelNumber)
    {
        return LoadLevelData(LevelNumberToName(levelNumber));
    }

    public int LoadLevelData(string levelName)
    {
        if (PlayerPrefs.HasKey(levelName))
        {
            return PlayerPrefs.GetInt(levelName);
        }
        return -1;
    }

    public void ClearSaveData()
    {
        PlayerPrefs.DeleteAll();
        Invoke_OnClearData();
    }

    public void Add_OnClearData(Action listener)
    {
        OnClearData += listener;
    }

    public void Remove_OnClearData(Action listener)
    {
        if (OnClearData != null) OnClearData -= listener;
    }

    private void Invoke_OnClearData()
    {
        if (OnClearData != null) OnClearData.Invoke();
    }
}
