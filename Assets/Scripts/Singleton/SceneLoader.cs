using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private ConstDictionary constDict;

    private static SceneLoader _instance;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }

    public void Start()
    {
        /*
         * ConstManager is in the same GameObject and could be found with a simple GetComponent
         * anyway, I can't be sure this will be the case in the final game, and every component is meant
         * to work wherever ConstManager is in the scene. Since the constDict reference is always 
         * retrieved in Start, just once for every component that needs it, the overhead should
         * be minimal even if a Find method is used.
         */
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene(constDict.MainMenuSceneName);
    }

    public void LoadLevel(int number)
    {
        SceneManager.LoadScene(constDict.LevelScenePrefix + number.ToString());
    }

    public void LoadTestLevel()
    {
        SceneManager.LoadScene(constDict.TestLevelSceneName);
    }

    public void LoadEndingScreen()
    {
        SceneManager.LoadScene(constDict.EndingScreenSceneName);
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene(constDict.CreditsSceneName);
    }
}
