using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgMusicController : MonoBehaviour
{
    private OptionsController options;
    private AudioSource bgMusic;

    public void Start()
    {
        options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        bgMusic = GetComponent<AudioSource>();
        options.Add_OnBgMusicVolumeChange(ChangeVolume);

    }

    public void OnDestroy()
    {
        if (options) options.Remove_OnBgMusicVolumeChange(ChangeVolume);
    }

    public void ChangeVolume()
    {
        bgMusic.volume = options.BgMusicVolume;
    }
}
