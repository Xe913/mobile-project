using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TouchController : MonoBehaviour
{
    private ConstDictionary constDict;
    private GlobalEventsHandler geh;

    private Vector3 _touchPoint;
    private bool _touchLocked;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalEventsHandler>();
    }

    public void Update()
    {
        if (!_touchLocked)
        {
            int touchIndex = GetFirstTouchIndex();
            if (touchIndex != -1) ManageTouchInput(Input.GetTouch(touchIndex));
        }
    }

    private int GetFirstTouchIndex()
    {
        for (int i = 0; i < Input.touchCount; i++)
            if (!EventSystem.current.IsPointerOverGameObject(Input.GetTouch(i).fingerId))
                return i;
        return -1;
    }

    private void ManageTouchInput(Touch touch)
    {
        _touchPoint = Camera.main.ScreenToWorldPoint(touch.position);
        switch (touch.phase)
        {
            case TouchPhase.Began:
                TouchStart(_touchPoint);
                break;
            case TouchPhase.Moved:
                TouchMove(_touchPoint);
                break;
            case TouchPhase.Ended:
                TouchEnd(_touchPoint);
                break;
        }
    }

    private void TouchStart(Vector3 touchPoint)
    {
        //stuff
        geh.Invoke_OnTouchStart(touchPoint);
    }

    private void TouchMove(Vector3 touchPoint)
    {
        //stuff
    }

    private void TouchEnd(Vector3 touchPoint)
    {
        geh.Invoke_OnTouchEnd(touchPoint);
        //stuff
    }
}
