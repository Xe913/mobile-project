using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreasureMap : MonoBehaviour
{
    [SerializeField] private List<LootTable> treasureTables;
    [SerializeField] private List<GameObject> parentObjects;
    [SerializeField] private int objective = 3;

    private ConstDictionary constDict;
    private GlobalEventsHandler geh;
    [Header("TEST")]
    [SerializeField] private SignalGenerator signalGenerator;
    [SerializeField] private List<GameObject> instancedTreasureObjects;
    private int found = 0;
    private bool initialized = false;

    public void Start()
    {
        if (!initialized) InitializeMap();
    }

    private void InitializeMap()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        instancedTreasureObjects = new List<GameObject>();
        foreach (GameObject parentObject in parentObjects)
        {
            if (!parentObject.GetComponent<LootSource>()) parentObjects.Remove(parentObject);
        }
        geh.Add_OnLootObtained(ObtainTreasure);
        geh.Add_OnTreasureSpawn(AddTreasureLocation);
        geh.Add_OnTreasureRemoved(RemoveTreasureLocation);
        initialized = true;
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnLootObtained(ObtainTreasure);
            geh.Remove_OnTreasureSpawn(AddTreasureLocation);
            geh.Remove_OnTreasureRemoved(RemoveTreasureLocation);
        }
    }

    public void SpawnTreasures(bool randomize)
    {
        if (!initialized) InitializeMap();
        int treasureIndex = 0;
        int parentObjectIndex;
        if (!randomize)
        {
            while (treasureIndex < treasureTables.Count && treasureIndex < parentObjects.Count)
            {
                AddTreasureToObject(treasureTables[treasureIndex], parentObjects[treasureIndex]);
                treasureIndex++;
            }
        }
        else
        {
            while (treasureIndex < treasureTables.Count && parentObjects.Count > 0)
            {
                parentObjectIndex = Random.Range(0, parentObjects.Count);
                AddTreasureToObject(treasureTables[treasureIndex], parentObjects[parentObjectIndex]);
                treasureIndex++;
                parentObjects.RemoveAt(parentObjectIndex);
            }
        }
    }

    private void AddTreasureToObject(LootTable treasureTable, GameObject parentObject)
    {
        LootSource source = parentObject.GetComponent<LootSource>();
        source.ChangeTable(treasureTable);
        List<MeshRenderer> removeWhenDiscovered = new List<MeshRenderer>();
        foreach (Transform child in parentObject.GetComponentsInChildren<Transform>())
        {
            if (child.gameObject.layer == LayerMask.NameToLayer(constDict.RadarLayer))
            {
                foreach (MeshRenderer mr in child.gameObject.GetComponents<MeshRenderer>())removeWhenDiscovered.Add(mr);
            }
        }
        signalGenerator.AddSignal(parentObject, treasureTable, removeWhenDiscovered);
    }

    private void AddTreasureLocation(GameObject treasureRelatedObject)
    {
        instancedTreasureObjects.Add(treasureRelatedObject);
    }

    private void RemoveTreasureLocation(GameObject treasureRelatedObject)
    {
        if (instancedTreasureObjects.Contains(treasureRelatedObject)) instancedTreasureObjects.Remove(treasureRelatedObject);
    }

    public Transform GetClosestTreasureLocation(Vector3 startingPosition)
    {
        if (instancedTreasureObjects.Count < 1) return null;
        float minDistance = float.MaxValue;
        Transform closestTreasureLocation = instancedTreasureObjects[0].transform;
        foreach(GameObject instancedTreasureObject in instancedTreasureObjects)
        {
            if(Vector3.Distance(startingPosition, instancedTreasureObject.transform.position) < minDistance)
            {
                minDistance = Vector3.Distance(startingPosition, instancedTreasureObject.transform.position);
                closestTreasureLocation = instancedTreasureObject.transform;
            }
        }
        return closestTreasureLocation;
    }

    private void ObtainTreasure(Loot treasure)
    {
        if (treasure is Treasure)
        {
            found++;
            if (found >= objective) geh.Invoke_OnWinConditionAchieved();
        }
    }
}
