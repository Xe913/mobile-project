using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootGenerator : MonoBehaviour
{
    private GlobalEventsHandler geh;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        geh.Add_OnAsteroidDestruction(AsteroidLoot);
        geh.Add_OnGameWon(DeactivateOnGameEnd);
        geh.Add_OnGameLost(DeactivateOnGameEnd);
    }
    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnAsteroidDestruction(AsteroidLoot);
            geh.Remove_OnGameWon(DeactivateOnGameEnd);
            geh.Remove_OnGameLost(DeactivateOnGameEnd);
        }
    }

    private void DeactivateOnGameEnd()
    {
        enabled = false;
    }

    private void AsteroidLoot(Asteroid asteroid, GameObject cause)
    {
        if (cause) //cause is null when the asteroid has been removed because its too far from the ship
        {
            LootSource source = asteroid.GetComponent<LootSource>();
            if (source && source.HasTable()) SpawnLoot(source);
        }
    }

    private void SpawnLoot(LootSource source)
    {
        Instantiate(source.GetLoot(), source.transform.position, source.transform.rotation);
    }
}
