using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectController : MonoBehaviour
{
    private OptionsController options;
    private Transform audioListenerLocation;

    public void Start()
    {
        options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        Camera[] cameraList;
        cameraList = FindObjectsOfType<Camera>();
        foreach(Camera camera in cameraList)
        {
            if (camera.gameObject.GetComponent<AudioListener>())
            {
                audioListenerLocation = camera.gameObject.GetComponent<AudioListener>().transform;
                break;
            }
        }
    }

    public void PlaySoundEffect(AudioClip clip)
    {
        PlaySoundEffect(clip, audioListenerLocation.position, options.SoundEffectsVolume);
    }

    public void PlaySoundEffect(AudioClip clip, Vector3 position)
    {
        PlaySoundEffect(clip, position, options.SoundEffectsVolume);
    }
    public void PlaySoundEffect(AudioClip clip, float volume)
    {
        PlaySoundEffect(clip, audioListenerLocation.position, volume);
    }

    public void PlaySoundEffect(AudioClip clip, Vector3 position, float volume)
    {
        if (clip) AudioSource.PlayClipAtPoint(clip, position, volume);
    }
}
