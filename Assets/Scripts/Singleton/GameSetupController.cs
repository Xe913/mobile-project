using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSetupController : MonoBehaviour
{
    private ConstDictionary constDict;
    private OptionsController options;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        SetupDifficulty();
    }

    private void SetupDifficulty()
    {
        TreasureMap map = GameObject.FindGameObjectWithTag(constDict.TreasureMapTag).GetComponent<TreasureMap>();
        HealthSystem shipHS = GameObject.FindGameObjectWithTag(constDict.ShipTag).GetComponent<HealthSystem>();
        Gun shipGun = GameObject.FindGameObjectWithTag(constDict.ShipTag).GetComponentInChildren<Gun>();

        bool hard = options.Difficulty == DifficultySettings.hard;
        map.SpawnTreasures(hard);
        shipHS.ChangeMaxHealth(hard ? Mathf.CeilToInt(shipHS.StartingHealth * 0.75f) : shipHS.StartingHealth);
        shipGun.HeatReductionPerSecond = hard ? Mathf.CeilToInt(shipGun.HeatReductionPerSecond / 5) : shipGun.HeatReductionPerSecond;
    }
}
