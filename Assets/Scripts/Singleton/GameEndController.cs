using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEndController : MonoBehaviour
{
    private GlobalEventsHandler geh;
    private SaveManager sm;

    private bool gameEnded;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sm = GameObject.FindGameObjectWithTag("GameController").GetComponent<SaveManager>();
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        geh.Add_OnWinConditionAchieved(GameWon);
        geh.Add_OnLoseConditionAchieved(GameLost);
    }

    public void OnDestroy()
    {
        geh.Remove_OnWinConditionAchieved(GameWon);
        geh.Remove_OnLoseConditionAchieved(GameLost);
    }

    private void GameWon()
    {
        if (!gameEnded)
        {
            gameEnded = true;
            sm.LevelWon();
            geh.Invoke_OnGameWon();
        }
    }

    private void GameLost()
    {
        if (!gameEnded)
        {
            gameEnded = true;
            geh.Invoke_OnGameLost();
        }
    }
}
