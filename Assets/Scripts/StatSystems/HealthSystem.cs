using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour
{
    [SerializeField] private int _startingHealth;
    [SerializeField] private bool _canTakeImpactDamage = true;
    [Header("DEBUG")]
    [SerializeField] private int _currentHealth;

    private Action<int, GameObject> onDamageTaken; //damage, cause

    public int StartingHealth { get => _startingHealth; }
    public bool CanTakeImpactDamage { get => _canTakeImpactDamage; set => _canTakeImpactDamage = value; }
    public int CurrentHealth { get => _currentHealth; }

    public void Start()
    {
        _currentHealth = _startingHealth;
    }

    public void ChangeMaxHealth(int newMaxHealth)
    {
        _startingHealth = newMaxHealth;
        _currentHealth = _startingHealth;
    }

    public void ProcessHit(int damage, GameObject cause)
    {
        if (_canTakeImpactDamage) _currentHealth = Mathf.Clamp(CurrentHealth - damage, 0, StartingHealth);
        else damage = 0;
        InvokeOnDamageTaken(damage, cause);
    }

    private void InvokeOnDamageTaken(int damage, GameObject cause)
    {
        if (onDamageTaken != null) onDamageTaken.Invoke(damage, cause);
    }

    public void Add_OnDamageTaken(Action<int, GameObject> listener)
    {
        onDamageTaken += listener;
    }

    public void Remove_OnDamageTaken(Action<int, GameObject> listener)
    {
        onDamageTaken -= listener;
    }
}
