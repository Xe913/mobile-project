using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImpactController : MonoBehaviour
{
    [SerializeField] private int _damageOnImpact;

    public int DamageOnImpact { get => _damageOnImpact; set => _damageOnImpact = value; }

    private void OnCollisionEnter(Collision collision)
    {
        HealthSystem collisionHS = collision.gameObject.GetComponentInChildren<HealthSystem>();
        if (!collisionHS) collisionHS = collision.gameObject.GetComponentInParent<HealthSystem>();
        if (collisionHS) collisionHS.ProcessHit(DamageOnImpact, gameObject);
    }
}
