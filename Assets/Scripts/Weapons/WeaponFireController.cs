using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class WeaponFireController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    protected GlobalEventsHandler geh;
    protected bool firing = false;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
    }

    protected abstract void InvokeStartFiringEvents();
    protected abstract void InvokeStopFiringEvents();

    public void OnPointerDown(PointerEventData eventData)
    {
        InvokeStartFiringEvents();
        firing = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        InvokeStopFiringEvents();
        firing = false;
    }
}
