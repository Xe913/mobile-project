using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Weapon
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float _bulletImpulse;
    [SerializeField] private float _bulletDestructionAfter = 30f;
    [SerializeField] private float fireCooldown;
    [SerializeField] private Transform rotatingHead;

    public float BulletImpulse { get => _bulletImpulse; set => _bulletImpulse = value; }
    public float BulletDestructionAfter { get => _bulletDestructionAfter; set => _bulletDestructionAfter = value; }

    private Transform shipLocation;

    public new void Start()
    {
        base.Start();
        shipLocation = GameObject.FindGameObjectWithTag(constDict.ShipTag).transform;
        ReadyToFire();
    }

    public void Update()
    {
        rotatingHead.LookAt(shipLocation);
    }

    public void FixedUpdate()
    {
        StartFiring();
    }

    public override void StartFiring()
    {
        if (CanFire() && CanSeeShip())
        {
            GunBullet firedBullet = FireBullet(bullet, _bulletImpulse).GetComponent<GunBullet>();
            firedBullet.SetupSelfdestruction(_range, _bulletDestructionAfter);
            PlayFireSound();
            readyToFire = false;
            FireCooldown(fireCooldown);
        }
    }

    RaycastHit hit;
    private bool CanSeeShip()
    {
        if (Physics.Raycast(firingPoint.position, firingPoint.forward, out hit, _range))
        {
            Debug.Log("collider " + hit.collider.gameObject + " | layer " + hit.collider.gameObject.layer + " | " + LayerMask.NameToLayer(constDict.ShipLayer));
            return hit.collider.gameObject.layer == LayerMask.NameToLayer(constDict.ShipLayer);
        }
        return false;
    }
}
