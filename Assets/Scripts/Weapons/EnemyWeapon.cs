using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWeapon : Weapon
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float _bulletImpulse;
    [SerializeField] private float _bulletDestrutionAfter = 30f;
    [SerializeField] private float _fireCooldown;

    public float BulletImpulse { get => _bulletImpulse; set => _bulletImpulse = value; }
    public float BulletDestrutionAfter { get => _bulletDestrutionAfter; set => _bulletDestrutionAfter = value; }
    public float FireRate { get => _fireCooldown; set => _fireCooldown = value; }

    public new void Start()
    {
        base.Start();
        ReadyToFire();
    }

    public override void StartFiring()
    {
        if (CanFire())
        {
            GunBullet firedBullet = FireBullet(bullet, _bulletImpulse).GetComponent<GunBullet>();
            firedBullet.SetupSelfdestruction(_range, _bulletDestrutionAfter);
            PlayFireSound();
            readyToFire = false;
            FireCooldown(_fireCooldown);
        }
    }
}
