using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FireRepairToolButton : WeaponFireController, IPointerDownHandler, IPointerUpHandler
{
    protected override void InvokeStartFiringEvents()
    {
        geh.Invoke_OnStartFiringRepairBeam();
    }

    protected override void InvokeStopFiringEvents()
    {
        geh.Invoke_OnStopFiringRepairBeam();
    }
}
