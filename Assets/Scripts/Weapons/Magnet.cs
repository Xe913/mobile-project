using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : Weapon
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float _bulletImpulse;
    [SerializeField] private float _returnVelocity;
    [SerializeField] private float _returnAfter;
    [SerializeField] private float _magnetCooldown;

    private bool fired;
    private bool _returning;

    public float BulletImpulse { get => _bulletImpulse; set => _bulletImpulse = value; }
    public float ReturnVelocity { get => _returnVelocity; set => _returnVelocity = value; }
    public float ReturnAfter { get => _returnAfter; set => _returnAfter = value; }
    public float MagnetCooldown { get => _magnetCooldown; set => _magnetCooldown = value; }
    public bool Returning { get => _returning; }

    public new void Start()
    {
        base.Start();
        ReadyToFire();
    }

    public override void StartFiring()
    {
        if (CanFire())
        {
            fired = true;
            MagnetBullet firedMagnet = FireBullet(bullet, _bulletImpulse).GetComponent<MagnetBullet>();
            firedMagnet.Weapon = this;
            firedMagnet.ReturnTo = transform;
            firedMagnet.ReturnVelocity = _returnVelocity;
            firedMagnet.SetupReturn(_range, _returnAfter);
            PlayFireSound();
            readyToFire = false;
            FireCooldown(_magnetCooldown);
        }
    }

    public override bool CanFire()
    {
        return !fired && readyToFire;
    }

    public void MagnetReturning()
    {
        _returning = true;
    }

    public void MagnetBack()
    {
        fired = false;
        _returning = false;
    }
}
