using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetBullet : Bullet
{
    [SerializeField] private ParticleSystem attractParticles;

    private ConstDictionary constDict;
    private MagnetTarget returnMechanic;
    private Magnet _weapon;
    private Transform _returnTo;
    private float _returnVelocity;

    private bool returning;

    public Magnet Weapon { get => _weapon; set => _weapon = value; }
    public Transform ReturnTo { get => _returnTo; set => _returnTo = value; }
    public float ReturnVelocity { get => _returnVelocity; set => _returnVelocity = value; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        returnMechanic = GetComponent<MagnetTarget>();
    }

    public void OnDestroy()
    {
        DestructionEffect();
    }

    public void SetupReturn(float range, float timer)
    {
        StartCoroutine(CheckMaxRange(range, TurnBack));
        StartCoroutine(CheckMaxLifetime(timer, TurnBack));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!returning)
        {
            Grab(collision.gameObject);
        }
        else
        {
            if (collision.gameObject.CompareTag(constDict.ShipTag)) Returned();
        }
    }

    private void TurnBack()
    {
        if (!returning)
        {
            returning = true;
            returnMechanic.Attract(_returnTo, _returnVelocity);
        }
    }

    private void Grab(GameObject grabbedObject)
    {
        MagnetTarget returningMagnet = grabbedObject.GetComponent<MagnetTarget>();
        if (returningMagnet)
        {
            returning = true;
            returningMagnet.Attract(_returnTo, _returnVelocity, attractParticles);

        }
        Returned();
    }

    private void Returned()
    {
        _weapon.MagnetBack();
        BulletDestruction();
    }
}
