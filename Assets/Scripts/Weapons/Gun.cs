using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : Weapon
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float _bulletImpulse;
    [SerializeField] private float _bulletDestructionAfter = 30f;
    [SerializeField] private float _fireRate;
    [SerializeField] private float _heatPerBullet;
    [SerializeField] private float _heatReductionPerSecond;
    [SerializeField] private float _freezeHeatReductionPerSecond;

    [Header("DEBUG")]
    [SerializeField] private float _currentHeat;
    private float currentHeatReduction;
    private bool coolingDown;

    private const float MAX_HEAT = 100;
    private const float OVER_HEAT = 120;

    public float BulletImpulse { get => _bulletImpulse; set => _bulletImpulse = value; }
    public float BulletDestrutionAfter { get => _bulletDestructionAfter; set => _bulletDestructionAfter = value; }
    public float FireRate { get => _fireRate; set => _fireRate = value; }
    public float HeatPerBullet { get => _heatPerBullet; set => _heatPerBullet = value; }
    public float HeatReductionPerSecond { get => _heatReductionPerSecond; set => _heatReductionPerSecond = value; }
    public float FreezeHeatReductionPerSecond { get => _freezeHeatReductionPerSecond; set => _freezeHeatReductionPerSecond = value; }
    public float CurrentHeat { get => _currentHeat; }

    public new void Start()
    {
        base.Start();
        _currentHeat = 0;
        currentHeatReduction = _heatReductionPerSecond;
        coolingDown = false;
        geh.Add_OnFreezeShip(Freeze);
        ReadyToFire();
    }

    public void OnDestroy()
    {
        if (geh) geh.Remove_OnFreezeShip(Freeze);
    }

    public void Update()
    {
        if(_currentHeat > 0) UpdateHeatLevel(-currentHeatReduction * Time.deltaTime);
        if (firing && readyToFire && _currentHeat < MAX_HEAT && !coolingDown)
        {
            GunBullet firedBullet = FireBullet(bullet, _bulletImpulse).GetComponent<GunBullet>();
            PlayFireSound();
            firedBullet.SetupSelfdestruction(_range, _bulletDestructionAfter);
            readyToFire = false;
            UpdateHeatLevel(_heatPerBullet);
            FireCooldown(_fireRate);
        }
    }

    private void UpdateHeatLevel(float update)
    {
        _currentHeat = Mathf.Clamp(_currentHeat + update, 0, OVER_HEAT);
    }

    public float CurrentHeatPercentage()
    {
        return CurrentHeat / MAX_HEAT;
    }

    private void Freeze()
    {
        if (!coolingDown)
        {
            coolingDown = true;
            StartCoroutine(FreezeHeatReduction());
        }
    }

    private IEnumerator FreezeHeatReduction()
    {
        currentHeatReduction = _freezeHeatReductionPerSecond;
        while (_currentHeat > 0) yield return null;
        currentHeatReduction = _heatReductionPerSecond;
        coolingDown = false;
    }
}
