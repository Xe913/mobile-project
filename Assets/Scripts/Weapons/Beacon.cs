using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Beacon : Weapon
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private float _bulletImpulse;
    [SerializeField] private float _bulletDestructionAfter = 30f;
    [SerializeField] private float _trailImpulse;
    [SerializeField] private float _trailDestructionAfter = 15f;
    [SerializeField] private float _trailMinDistanceFromTarget = 10f;
    [SerializeField] private int _maxCharges;
    [SerializeField] private float beaconCooldown;
    [Header("DEBUG")]
    [SerializeField] private int _currentCharges;

    public float BulletImpulse { get => _bulletImpulse; set => _bulletImpulse = value; }
    public float BulletDestructionAfter { get => _bulletDestructionAfter; set => _bulletDestructionAfter = value; }
    public float TrailImpulse { get => _trailImpulse; set => _trailImpulse = value; }
    public float TrailDestructionAfter { get => _trailDestructionAfter; set => _trailDestructionAfter = value; }
    public float TrailMinDistanceFromTarget { get => _trailMinDistanceFromTarget; set => _trailMinDistanceFromTarget = value; }
    public int MaxCharges { get => _maxCharges; }
    public int CurrentCharges { get => _currentCharges; }

    public new void Start()
    {
        base.Start();
        readyToFire = true;
    }

    public override void StartFiring()
    {
        if (CanFire())
        {
            BeaconBullet firedBeacon = FireBullet(bullet, _bulletImpulse).GetComponent<BeaconBullet>();
            firedBeacon.TrailImpulse = _trailImpulse;
            firedBeacon.TrailLifetime = _trailDestructionAfter;
            firedBeacon.TrailMinDistanceFromTarget = _trailMinDistanceFromTarget;
            firedBeacon.SetupTrailSpawn(_range, _bulletDestructionAfter);
            PlayFireSound();
            _currentCharges = Mathf.Max(_currentCharges - 1, 0);
            readyToFire = false;
            FireCooldown(beaconCooldown);
        }
    }

    public void AddCharges(int charges)
    {
        _currentCharges = Mathf.Min(_currentCharges + charges, _maxCharges);
    }

    public override bool CanFire()
    {
        return _currentCharges > 0 && readyToFire;
    }
}
