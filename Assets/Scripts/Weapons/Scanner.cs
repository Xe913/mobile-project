using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scanner : Weapon
{
    [SerializeField] private LayerMask scannerMask;
    [SerializeField] private float _scanCooldown;
    [SerializeField] private float scanFirstDelay;
    [SerializeField] private float scanInbetweenDelay;

    private WaitForSeconds firstDelay;
    private WaitForSeconds inbetweenDelay;

    public float ScanCooldown { get => _scanCooldown; set => _scanCooldown = value; }

    public new void Start()
    {
        base.Start();
        firstDelay = new WaitForSeconds(scanFirstDelay);
        inbetweenDelay = new WaitForSeconds(scanInbetweenDelay);
        ReadyToFire();
    }

    public override void StartFiring()
    {
        if(CanFire())
        {
            Scan();
            readyToFire = false;
            FireCooldown(_scanCooldown);
        }
    }

    private void Scan()
    {
        Collider[] hits = Physics.OverlapSphere(firingPoint.transform.position, _range, scannerMask, QueryTriggerInteraction.Collide);
        List<Signal> signalList = new List<Signal>();
        Signal scannedSignal;
        foreach(Collider hit in hits)
        {
            scannedSignal = hit.gameObject.GetComponentInParent<Signal>();
            if (scannedSignal) signalList.Add(scannedSignal);
        }
        StartCoroutine(DiscoverSignals(signalList));

    }

    private IEnumerator DiscoverSignals(List<Signal> signals)
    {
        PlayFireSound();
        yield return firstDelay;
        foreach (Signal signal in signals)
        {
            yield return inbetweenDelay;
            signal.SeeOnRadar();
        }
    }
}
