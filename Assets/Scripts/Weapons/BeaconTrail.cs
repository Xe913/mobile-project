using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconTrail : Bullet
{
    private Rigidbody rb;
    private Transform targetLocation;
    private float minDistanceFromTarget;

    public void Update()
    {
        if (!targetLocation || Vector3.Distance(targetLocation.position, transform.position) < minDistanceFromTarget) BulletDestruction();
    }

    public void SetupSelfdestruction(float timer, float minDistanceFromTarget)
    {
        this.minDistanceFromTarget = minDistanceFromTarget;
        StartCoroutine(CheckMaxLifetime(timer, BulletDestruction));
    }

    public void FireTrail(Transform targetLocation, float impulse)
    {
            this.targetLocation = targetLocation;
            transform.LookAt(this.targetLocation);
            rb = GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * impulse, ForceMode.Impulse);
    }
}
