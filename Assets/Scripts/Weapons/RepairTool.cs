using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairTool : Weapon
{
    [SerializeField] private GameObject beam;
    [SerializeField] private float repairIntervalLength;

    private RepairToolBeam firedBeam;

    public new void Start()
    {
        base.Start();
        geh.Add_OnStartFiringRepairBeam(StartFiring);
        geh.Add_OnStopFiringRepairBeam(StopFiring);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnStartFiringRepairBeam(StartFiring);
            geh.Remove_OnStopFiringRepairBeam(StopFiring);
        }
    }

    public override void StartFiring()
    {
        if (!firing)
        {
            firedBeam = Instantiate(beam, firingPoint).GetComponent<RepairToolBeam>();
            firedBeam.Setup(_damage, repairIntervalLength, _range);
        }
        base.StartFiring();
    }

    public override void StopFiring()
    {
        if (firing) Destroy(firedBeam.gameObject);
        base.StopFiring();
    }
}
