using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetTarget : MonoBehaviour
{
    private Rigidbody rb;
    private bool attracted;

    public void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    public void Attract(Transform attractedBy, float attractionSpeed, ParticleSystem particles = null)
    {
        if (!attracted)
        {
            attracted = true;
            rb.isKinematic = false;
            if (particles)
            {
                Instantiate(particles, transform);
            }
            StartCoroutine(AttractCoroutine(attractedBy, attractionSpeed));
        }
    }

    private IEnumerator AttractCoroutine(Transform attractedBy, float attractionSpeed)
    {
        Magnet magnet = attractedBy.gameObject.GetComponent<Magnet>();
        magnet.MagnetReturning();
        while (transform.position != attractedBy.position && attracted)
        {
            rb.velocity = (attractedBy.position - transform.position).normalized * attractionSpeed;
            yield return null;
        }
        magnet.MagnetBack();
    }
}
