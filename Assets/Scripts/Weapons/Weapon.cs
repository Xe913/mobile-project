using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField] protected int _damage;
    [SerializeField] protected float _range;
    [SerializeField] private AudioClip _fireSound;
    [SerializeField] protected Transform firingPoint;

    protected ConstDictionary constDict;
    protected GlobalEventsHandler geh;
    protected SoundEffectController sfxController;
    protected bool firing;
    protected bool readyToFire;

    protected delegate void EndOfCooldownEvent();

    protected float Damage { get => _damage; }
    public float Range { get => _range; }
    protected AudioClip FireSound { get => _fireSound; set => _fireSound = value; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
        if (!firingPoint) firingPoint = transform;
    }

    public virtual void StartFiring()
    {
        firing = true;
    }

    public virtual void StopFiring()
    {
        firing = false;
    }

    public virtual bool CanFire()
    {
        return readyToFire;
    }

    protected GameObject FireBullet(GameObject bullet, float forceOnSpawn)
    {
        GameObject instancedBullet = Instantiate(bullet, firingPoint.position, firingPoint.rotation);
        instancedBullet.transform.LookAt(firingPoint.forward);
        if (Damage != 0 && instancedBullet.GetComponent<ImpactController>())
            instancedBullet.GetComponent<ImpactController>().DamageOnImpact = _damage;
        instancedBullet.GetComponent<Rigidbody>().AddForce(firingPoint.forward * forceOnSpawn, ForceMode.Impulse);
        return instancedBullet;
    }

    protected void PlayFireSound()
    {
        sfxController.PlaySoundEffect(_fireSound);
    }

    protected void FireCooldown(float cooldown)
    {
        StartCoroutine(StartCooldown(cooldown, ReadyToFire));
    }

    protected IEnumerator StartCooldown(float cooldown, EndOfCooldownEvent endOfCooldownEvent)
    {
        yield return new WaitForSeconds(cooldown);
        endOfCooldownEvent();
    }

    protected void ReadyToFire()
    {
        readyToFire = true;
    }
}
