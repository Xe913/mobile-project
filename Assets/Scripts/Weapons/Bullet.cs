using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Bullet : MonoBehaviour
{
    protected bool destroyed;

    protected IEnumerator CheckMaxRange(float range, Action maxRangeEvent)
    {
        float crossedDistance = 0;
        Vector3 previousPosition = transform.position;
        while (crossedDistance < range)
        {
            crossedDistance += Vector3.Distance(transform.position, previousPosition);
            previousPosition = transform.position;
            yield return null;
        }
        if (maxRangeEvent != null) maxRangeEvent.Invoke();
    }

    protected IEnumerator CheckMaxLifetime(float timer, Action maxLifetimeEvent)
    {
        yield return new WaitForSeconds(timer);
        if (maxLifetimeEvent != null) maxLifetimeEvent.Invoke();
    }
    protected void BulletDestruction()
    {
        if (!destroyed) DestructionEffect();
        destroyed = true;
        Destroy(gameObject);
    }

    protected virtual void DestructionEffect() { }
}
