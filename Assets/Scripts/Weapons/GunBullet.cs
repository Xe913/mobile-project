using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunBullet : Bullet
{
    public void SetupSelfdestruction(float range, float timer)
    {
        StartCoroutine(CheckMaxRange(range, BulletDestruction));
        StartCoroutine(CheckMaxLifetime(timer, BulletDestruction));
    }

    private void OnCollisionEnter(Collision collision)
    {
        BulletDestruction();
    }
}
