using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairToolBeam : MonoBehaviour
{
    private int repairPerInterval;
    private float intervalLength;
    private float range;

    private WaitForSeconds interval;

    public void Start()
    {
        interval = new WaitForSeconds(intervalLength);
        StartCoroutine(Repair());
    }

    public void Setup(int repairPerInterval, float intervalLength, float range)
    {
        this.repairPerInterval = repairPerInterval;
        this.intervalLength = intervalLength;
        this.range = range;
    }

    private RaycastHit hit;
    private ShipDamage damageSpot;
    private IEnumerator Repair()
    {
        while (true)
        {
            if (Physics.Raycast(transform.position, transform.forward, out hit, range, 1, QueryTriggerInteraction.Collide))
            {
                damageSpot = hit.collider.gameObject.GetComponent<ShipDamage>();
                if (damageSpot) damageSpot.ReduceDamage(repairPerInterval);
            }
            yield return interval;
        }
    }
}
