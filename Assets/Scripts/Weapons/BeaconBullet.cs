using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeaconBullet : Bullet
{
    [SerializeField] private GameObject trail;

    private ConstDictionary constDict;
    private TreasureMap map;
    private float _trailImpulse;
    private float _trailLifetime;
    private float _trailMinDistanceFromTarget;

    public float TrailImpulse { get => _trailImpulse; set => _trailImpulse = value; }
    public float TrailLifetime { get => _trailLifetime; set => _trailLifetime = value; }
    public float TrailMinDistanceFromTarget { get => _trailMinDistanceFromTarget; set => _trailMinDistanceFromTarget = value; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        map = GameObject.FindGameObjectWithTag(constDict.TreasureMapTag).GetComponent<TreasureMap>();
    }

    public void SetupTrailSpawn(float range, float timer)
    {
        StartCoroutine(CheckMaxRange(range, SpawnTrail));
        StartCoroutine(CheckMaxLifetime(timer, SpawnTrail));
    }

    public void SpawnTrail()
    {
        Transform targetLocation = map.GetClosestTreasureLocation(transform.position);
        if (targetLocation != null)
        {
            BeaconTrail spawnedTrail = Instantiate(trail, transform.position, transform.rotation).GetComponent<BeaconTrail>();
            spawnedTrail.FireTrail(targetLocation, _trailImpulse);
            spawnedTrail.SetupSelfdestruction(_trailLifetime, _trailMinDistanceFromTarget);
        }
        BulletDestruction();
    }

    private void OnCollisionEnter(Collision collision)
    {
        BulletDestruction();
    }
}
