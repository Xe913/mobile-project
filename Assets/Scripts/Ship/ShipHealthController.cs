using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipHealthController : MonoBehaviour
{
    [SerializeField] private AudioClip damageTakenSound;

    private GlobalEventsHandler geh;
    private SoundEffectController sfxController;
    private HealthSystem hs;


    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
        hs = GetComponent<HealthSystem>();
        geh.Add_OnGameWon(MakeShipInvincible);
        geh.Add_OnGameLost(MakeShipInvincible);
        hs.Add_OnDamageTaken(DamageTaken);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(MakeShipInvincible);
            geh.Remove_OnGameLost(MakeShipInvincible);
        }
        if (hs) hs.Remove_OnDamageTaken(DamageTaken);
    }

    public void DamageTaken(int damage, GameObject source)
    {
        if (damage > 0)
        {
            sfxController.PlaySoundEffect(damageTakenSound);
        }
        CheckShipDestroyed();
    }

    public void CheckShipDestroyed()
    {
        if (hs.CurrentHealth <= 0) geh.Invoke_OnLoseConditionAchieved();
    }

    public void MakeShipInvincible()
    {
        hs.CanTakeImpactDamage = false;
    }
}
