using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwapDirection { previous, next }

public class WeaponController : MonoBehaviour
{
    [SerializeField] private List<GameObject> weapons;
    [SerializeField] private bool autoUnlock;

    private GlobalEventsHandler geh;
    private List<Weapon> weaponList;
    [SerializeField] private Weapon _activeWeapon;
    private int _activetWeaponIndex = 0;
    private bool _swapEnabled = true;

    public Weapon ActiveWeapon { get => _activeWeapon; }
    public int ActivetWeaponIndex { get => _activetWeaponIndex; }
    public bool SwapEnabled { get => _swapEnabled; }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        weaponList = new List<Weapon>();
        foreach (GameObject weaponGO in weapons) weaponList.Add(weaponGO.GetComponent<Weapon>());
        _activeWeapon = weaponList[_activetWeaponIndex];
        geh.Add_OnStartFiringShipWeapon(StartFiringActiveWeapon);
        geh.Add_OnStopFiringShipWeapon(StopFiringActiveWeapon);
        geh.Add_OnWeaponSwapInput(SwapWeapon);
        StartCoroutine(SetupFirstWeapon());
    }

    private IEnumerator SetupFirstWeapon()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        geh.Invoke_OnWeaponSwapped(_activeWeapon);
    }

    public void OnDestroy()
    {
        geh.Remove_OnStartFiringShipWeapon(StartFiringActiveWeapon);
        geh.Remove_OnStopFiringShipWeapon(StopFiringActiveWeapon);
        geh.Remove_OnWeaponSwapInput(SwapWeapon);
    }

    public void StartFiringActiveWeapon()
    {
        _activeWeapon.StartFiring();
    }

    public void StopFiringActiveWeapon()
    {
        _activeWeapon.StopFiring();
    }

    private void SwapWeapon(SwapDirection direction)
    {
        if (_swapEnabled)
        {
            _swapEnabled = false;
            switch (direction)
            {
                case SwapDirection.next:
                    NextWeapon();
                    break;
                case SwapDirection.previous:
                    PreviousWeapon();
                    break;
            }
            /*
             * If there's no animation on weapon swap, the controller itself confirms the completed
             * swap after changing the active weapon.
             * If there is another component that manages the swap animation, that one should
             * declare the end of the swap by calling this script CompleteSwap() method
             */
            if (autoUnlock) geh.Invoke_OnWeaponSwapped(_activeWeapon);
            else DisableSwap();

        }
    }

    private void NextWeapon()
    {
        _activetWeaponIndex = (_activetWeaponIndex + 1) % weaponList.Count;
        _activeWeapon = weaponList[_activetWeaponIndex];
    }

    private void PreviousWeapon()
    {
        _activetWeaponIndex = _activetWeaponIndex > 0 ? _activetWeaponIndex - 1 : weaponList.Count - 1;
        _activeWeapon = weaponList[Mathf.Abs(_activetWeaponIndex)];
    }

    public void EnableSwap()
    {
        _swapEnabled = true;
    }

    public void DisableSwap()
    {
        _swapEnabled = false;
    }

    public void CompleteSwap()
    {
        geh.Invoke_OnWeaponSwapped(_activeWeapon);
        EnableSwap();
    }
}
