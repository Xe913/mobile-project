using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{
    [SerializeField] private Camera referenceCamera;
    [SerializeField] private LayerMask detectLayers;

    private float _aimRange;
    private float _aimedObjectDistance;

    public float AimRange { get => _aimRange; }
    public float AimedObjectDistance { get => _aimedObjectDistance; }

    public void Start()
    {
        _aimRange = referenceCamera.farClipPlane - (transform.position - referenceCamera.transform.position).magnitude;
    }

    private RaycastHit hit;
    public void Update()
    {
        if(Physics.Raycast(transform.position, transform.forward, out hit, _aimRange, detectLayers))
            _aimedObjectDistance = Vector3.Distance(transform.position, hit.point);
        else
            _aimedObjectDistance = -1;
    }
}
