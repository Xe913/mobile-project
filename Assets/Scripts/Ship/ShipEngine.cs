using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipEngine : MonoBehaviour
{
    [SerializeField] private Vector2 rotationSpeed = Vector2.one;
    [SerializeField] private float forwardSpeed = 1;
    [SerializeField] private float backwardSpeed = 1;
    [Range(0, 1)]
    [SerializeField] private float freezeSpeedReduction;
    [Range(0, 1)]
    [SerializeField] private float speedRecoverPerSecond;

    private GlobalEventsHandler geh;
    private Rigidbody rb;
    private float movementInput;
    private Vector2 rotationInput;
    private float defaultForwardSpeed;
    private float defaultBackwardSpeed;
    [Header("DEBUG")]
    [SerializeField] private float currentVelocity;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        rb = GetComponent<Rigidbody>();
        defaultForwardSpeed = forwardSpeed;
        defaultBackwardSpeed = backwardSpeed;
        geh.Add_OnShipMovementInput(UpdateMovementInput);
        geh.Add_OnShipRotationInput(UpdateRotationInput);
        geh.Add_OnFreezeShip(Freeze);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnShipMovementInput(UpdateMovementInput);
            geh.Remove_OnShipRotationInput(UpdateRotationInput);
            geh.Remove_OnFreezeShip(Freeze);
        }
    }

    public void Update()
    {
        targetRotation.x -= rotationInput.y * rotationSpeed.y * Time.deltaTime;
        targetRotation.y += rotationInput.x * rotationSpeed.x * Time.deltaTime;
        //horizontalRotationComponent.rotation = Quaternion.Euler(0f, targetRotation.y, 0f);
        //verticalRotationComponent.localRotation = Quaternion.Euler(targetRotation.x, 0f, 0f);
        transform.Rotate(-rotationInput.y * rotationSpeed.y * Time.deltaTime, rotationInput.x * rotationSpeed.x * Time.deltaTime, 0f);
    }

    public void FixedUpdate()
    {
        if (movementInput > 0) rb.AddForce(transform.forward * movementInput * forwardSpeed, ForceMode.Acceleration);
        else rb.AddForce(transform.forward * movementInput * backwardSpeed, ForceMode.Acceleration);
        currentVelocity = rb.velocity.magnitude;
    }

    private void UpdateMovementInput(float movementInput)
    {
        if (this.movementInput == 0) rb.velocity = Vector3.zero; 
        this.movementInput = movementInput;
    }

    private Vector3 targetRotation;
    private void UpdateRotationInput(Vector2 rotationInput)
    {
        if (this.rotationInput.magnitude == 0) rb.angularVelocity = Vector3.zero;
        this.rotationInput.x = rotationInput.x;
        this.rotationInput.y = rotationInput.y;
    }

    private void Freeze()
    {
        Debug.Log("freeze?");
        forwardSpeed = defaultForwardSpeed * freezeSpeedReduction;
        backwardSpeed = defaultBackwardSpeed * freezeSpeedReduction;
        StartCoroutine(RecoverSpeed());
    }

    private IEnumerator RecoverSpeed()
    {
        bool forwardSpeedRecovered = false;
        bool backwardSpeedRecovered = false;
        while(!(forwardSpeedRecovered && backwardSpeedRecovered))
        {
            if (forwardSpeed < defaultForwardSpeed)
                forwardSpeed = Mathf.Min(forwardSpeed + (defaultForwardSpeed * speedRecoverPerSecond * Time.deltaTime), defaultForwardSpeed);
            else
                forwardSpeedRecovered = true;
            if (backwardSpeed < defaultBackwardSpeed)
                backwardSpeed = Mathf.Min(backwardSpeed + (defaultBackwardSpeed * speedRecoverPerSecond * Time.deltaTime), defaultBackwardSpeed);
            else backwardSpeedRecovered = true;
            yield return null;
        }
    }
}
