using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SignalGenerator : MonoBehaviour
{
    [SerializeField] GameObject signal;
    [SerializeField] LootTable signalLootTable;
    [Range(0, 1)]
    [SerializeField] private float percentageProgression;

    private GlobalEventsHandler geh;
    private float currentSignalSpawnPercentage = 0;

    private static SignalGenerator _instance;

    public void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        geh.Add_OnAsteroidSpawn(TryAddSignalOnAsteroidSpawn);
        geh.Add_OnGameWon(DeactivateOnGameEnd);
        geh.Add_OnGameLost(DeactivateOnGameEnd);
    }

    private void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnAsteroidSpawn(TryAddSignalOnAsteroidSpawn);
            geh.Remove_OnGameWon(DeactivateOnGameEnd);
            geh.Remove_OnGameLost(DeactivateOnGameEnd);
        }
    }
    private void DeactivateOnGameEnd()
    {
        enabled = false;
    }

    private void TryAddSignalOnAsteroidSpawn(Asteroid asteroid)
    {
        AddToTrackSpawnPercentage(percentageProgression);
        if (asteroid.SignalAvailable && RollToAddSignal()) AddSignal(asteroid.gameObject, signalLootTable, asteroid.RadarShadow.GetComponents<MeshRenderer>());
    }

    private void AddToTrackSpawnPercentage(float addPercentage)
    {
        currentSignalSpawnPercentage = Mathf.Clamp(currentSignalSpawnPercentage + addPercentage, 0, 1);
    }

    private bool RollToAddSignal()
    {
        float roll = Random.Range(0f, 1f);
        return roll < currentSignalSpawnPercentage;
    }
    public Signal AddSignal(GameObject parentObject, LootTable signalLootTable, List<MeshRenderer> removeWhenDiscovered)
    {
        return AddSignal(parentObject, signalLootTable, removeWhenDiscovered.ToArray());
    }

    public Signal AddSignal(GameObject parentObject, LootTable signalLootTable, MeshRenderer[] removeWhenDiscovered)
    {
        Signal addedSignal = InstantiateSignal(parentObject);
        addedSignal.SetupSignal(parentObject, signalLootTable, removeWhenDiscovered);
        ResetSignalSpawnPercentage();
        return addedSignal;
    }

    private Signal InstantiateSignal(GameObject addTo)
    {
        return Instantiate(signal, addTo.transform).GetComponent<Signal>();
    }

    private void ResetSignalSpawnPercentage()
    {
        currentSignalSpawnPercentage = 0;
    }
}
