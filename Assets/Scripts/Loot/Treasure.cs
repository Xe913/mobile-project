using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Treasure : Loot
{
    public new void Start()
    {
        base.Start();
        geh.Invoke_OnTreasureSpawn(gameObject);
    }

    public void OnDestroy()
    {
        geh.Invoke_OnTreasureRemoved(gameObject);
    }
}
