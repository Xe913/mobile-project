using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootSource : MonoBehaviour
{
    [SerializeField] private LootTable _table;

    private GlobalEventsHandler geh;

    public LootTable Table { get => _table; }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
    }

    public void OnDestroy()
    {
        if (CanDropTreasure()) geh.Invoke_OnTreasureRemoved(gameObject);
    }

    public bool HasTable()
    {
        return _table != null && _table.ContainedLoot != null && _table.ContainedLoot.Count > 0;
    }

    public void ChangeTable(LootTable newTable)
    {
        bool hadTreasure = CanDropTreasure();
        _table = newTable;
        //Change table can be called during the same frame a loot source is instanced,
        //for example when a signal-enabled or treasure asteroid is spawned.
        //In these cases, Start hasn't been called yet and the Global Event Handler reference is still null
        if (!geh)
        {
            ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
            geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        }
        if (!hadTreasure && CanDropTreasure()) geh.Invoke_OnTreasureSpawn(gameObject);
        if (hadTreasure && !CanDropTreasure()) geh.Invoke_OnTreasureRemoved(gameObject);
    }

    public GameObject GetLoot()
    {
        if (HasTable()) return _table.ContainedLoot[Random.Range(0, _table.ContainedLoot.Count)];
        return null;
    }

    public bool CanDropTreasure()
    {
        if (!HasTable()) return false;
        foreach (GameObject containedLoot in _table.ContainedLoot)
        {
            if (containedLoot.GetComponent<Treasure>()) return true;
        }
        return false;
    }
}
