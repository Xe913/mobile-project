using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Signal : MonoBehaviour
{
    [SerializeField] private AudioClip discoverSound;

    private ConstDictionary constDict;
    private SoundEffectController sfxController;
    private bool onRadar;

    private MeshRenderer[] addWhenDiscovered;
    private MeshRenderer[] removeWhenDiscovered;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
        addWhenDiscovered = GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer mr in addWhenDiscovered) mr.enabled = false;
    }

    public void SetupSignal(GameObject setupIn, LootTable loot = null, MeshRenderer[] removeWhenDiscovered = null)
    {
        if(loot != null) setupIn.GetComponent<LootSource>().ChangeTable(loot);
        if (removeWhenDiscovered != null && removeWhenDiscovered.Length > 0) this.removeWhenDiscovered = removeWhenDiscovered;

    }

    public void SeeOnRadar()
    {
        if (!onRadar)
        {
            if (addWhenDiscovered != null) foreach (MeshRenderer addMR in addWhenDiscovered) addMR.enabled = true;
            if (removeWhenDiscovered != null) foreach (MeshRenderer removeMR in removeWhenDiscovered) removeMR.enabled = false;
            onRadar = true;
        }
        sfxController.PlaySoundEffect(discoverSound);
    }
}
