using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Loot : MonoBehaviour
{
    [SerializeField] protected GameObject _collectedModel;
    [SerializeField] protected AudioClip collectedSound;

    protected ConstDictionary constDict;
    protected GlobalEventsHandler geh;
    protected SoundEffectController sfxController;

    public GameObject CollectedModel { get => _collectedModel; set => _collectedModel = value; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(constDict.ShipTag))
        {
            sfxController.PlaySoundEffect(collectedSound);
            geh.Invoke_OnLootObtained(this);
            Destroy(gameObject);
        }
    }
}
