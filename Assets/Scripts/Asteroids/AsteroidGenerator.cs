using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidGenerator : MonoBehaviour
{
    [SerializeField] private AsteroidConfig asteroidConfig;
    [SerializeField] private int asteroidsOnLevelStart;
    [SerializeField] private int maxAsteroids;
    [SerializeField] private float spawnDelay;
    [SerializeField] private float minDistanceFromShip;
    [SerializeField] private float maxDistanceFromShip;
    [SerializeField] private Camera[] excludeIfVisible;
    [SerializeField] private float _despawnDistance;
    [SerializeField] private int maxSpawnTries;

    protected GlobalEventsHandler geh;
    private Transform shipLocation;
    [SerializeField] private int instancedAsteroids = 0;
    private float defaultDelay;

    public float DespawnDistance { get => _despawnDistance; }

    public void Awake()
    {
        defaultDelay = spawnDelay;
        maxSpawnTries = Mathf.Abs(maxSpawnTries);
    }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        shipLocation = GameObject.FindGameObjectWithTag(constDict.ShipTag).transform;
        geh.Add_OnGameWon(DeactivateOnGameEnd);
        geh.Add_OnGameLost(DeactivateOnGameEnd);
        StartCoroutine(ReachPopulation(asteroidsOnLevelStart));
        StartCoroutine(CyclicSpawn());
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(DeactivateOnGameEnd);
            geh.Remove_OnGameLost(DeactivateOnGameEnd);
        }
    }
    private void DeactivateOnGameEnd()
    {
        enabled = false;
    }

    public void SpawnRandomAsteroid()
    {
        GameObject randomAsteroid = asteroidConfig.Models[UnityEngine.Random.Range(0, asteroidConfig.Models.Count)];
        Vector3 randomSpawnPosition = Vector3.zero;
        Quaternion randomSpawnRotation;
        bool spawnPointOk = true;
        for (int i = 0; i < maxSpawnTries; i++)
        {
            randomSpawnPosition = shipLocation.position + UnityEngine.Random.insideUnitSphere * maxDistanceFromShip;
            if (Vector3.Distance(shipLocation.position, randomSpawnPosition) < minDistanceFromShip)
            {
                spawnPointOk = false;
            }
            else
            {
                foreach (Camera camera in excludeIfVisible)
                {
                    spawnPointOk = !PointInsideCameraView(randomSpawnPosition, camera);
                    if (!spawnPointOk) break;
                }
            }
            if (spawnPointOk) break;
        }
        if (spawnPointOk)
        {
            randomSpawnRotation = Quaternion.Euler(
                UnityEngine.Random.Range(0f, 360f),
                UnityEngine.Random.Range(0f, 360f),
                UnityEngine.Random.Range(0f, 360f));
            SpawnAsteroid(randomAsteroid, randomSpawnPosition, randomSpawnRotation);
        }
    }

    public bool PointInsideCameraView(Vector3 point, Camera camera)
    {
        Vector3 viewportPoint = camera.WorldToViewportPoint(point);
        return viewportPoint.x > -1 &&
            viewportPoint.x < 1 &&
            viewportPoint.y > -1 &&
            viewportPoint.y < 1 &&
            viewportPoint.z > 0;
    }

    public GameObject SpawnAsteroid(GameObject asteroidToSpawn, Vector3 spawnPosition, Quaternion spawnRotation)
    {
        Asteroid asteroidComponent = asteroidToSpawn.GetComponent<Asteroid>();
        if (asteroidComponent)
        {
            Collider[] surroundingObjects = Physics.OverlapSphere(spawnPosition, asteroidComponent.RequiredSpace);
            if(surroundingObjects.Length < 1)
            {
                GameObject spawnedAsteroidGO = Instantiate(asteroidToSpawn, spawnPosition, spawnRotation);
                Asteroid spawnedAsteroid = spawnedAsteroidGO.GetComponent<Asteroid>();
                spawnedAsteroid.AssignSpawner(gameObject);
                instancedAsteroids++;
                geh.Invoke_OnAsteroidSpawn(spawnedAsteroid);
                return spawnedAsteroidGO;
            }
        }
        return null;
    }

    public void AsteroidDestroyed(Asteroid asteroid, GameObject cause)
    {
        instancedAsteroids--;
        if (cause) //cause is null when the asteroid has been removed because its too far from the ship
        {
            geh.Invoke_OnAsteroidDestruction(asteroid, cause);
        }
        else
        {
            SpawnRandomAsteroid();
        }
    }

    private IEnumerator ReachPopulation(int targetPopulation)
    {
        spawnDelay = 0;
        while (instancedAsteroids < targetPopulation) yield return null;
        spawnDelay = defaultDelay;
    }

    private WaitForEndOfFrame waitFrame;
    private IEnumerator CyclicSpawn()
    {
        while (enabled)
        {
            if (instancedAsteroids < maxAsteroids) SpawnRandomAsteroid();
            yield return waitFrame;
            yield return new WaitForSeconds(spawnDelay);
        }
    }
}
