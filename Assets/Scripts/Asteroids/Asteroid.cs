using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{
    [SerializeField] protected GameObject _body;
    [SerializeField] protected GameObject _radarShadow;
    [SerializeField] protected float _requiredSpace;
    [SerializeField] protected float distanceCheckDelay;
    [SerializeField] protected float minSpeed;
    [SerializeField] protected float maxSpeed;
    [SerializeField] protected float minRotation;
    [SerializeField] protected float maxRotation;
    [SerializeField] protected bool _signalCanBeAddedOnSpawn;
    [SerializeField] protected ParticleSystem explosion;
    [SerializeField] protected AudioClip explosionSound;

    protected ConstDictionary constDict;
    protected Transform shipLocation;
    protected SoundEffectController sfxController;

    protected Rigidbody rb;
    protected HealthSystem hs;
    protected AsteroidGenerator generator;
    protected float despawnDistance;

    public GameObject Body { get => _body; }
    public GameObject RadarShadow { get => _radarShadow; }
    public float RequiredSpace { get => _requiredSpace; }
    public bool SignalAvailable { get => _signalCanBeAddedOnSpawn; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sfxController = GameObject.FindGameObjectWithTag(constDict.AudioController).GetComponent<SoundEffectController>();
        shipLocation = GameObject.FindGameObjectWithTag(constDict.ShipTag).transform;
        distanceCheckDelay = Mathf.Max(distanceCheckDelay, 0);
        rb = GetComponent<Rigidbody>();
        hs = GetComponent<HealthSystem>();
        hs.Add_OnDamageTaken(CheckExplosion);
        AddInitialForce();
    }

    public void OnDestroy()
    {
        if (hs) hs.Remove_OnDamageTaken(CheckExplosion);
    }

    protected virtual void AddInitialForce()
    {
        rb.AddForce(transform.forward * Random.Range(minSpeed, maxSpeed), ForceMode.VelocityChange);
        rb.AddTorque(
            new Vector3(Random.Range(minRotation, maxRotation),
                Random.Range(minRotation, maxRotation),
                Random.Range(minRotation, maxRotation)),
            ForceMode.VelocityChange);
    }

    public void AssignSpawner(GameObject spawner)
    {
        generator = spawner.GetComponent<AsteroidGenerator>();
        if (generator) StartCoroutine(CheckDistance());
        else Destroy(gameObject);
    }

    //triggered on hit by HealthSystem
    private void CheckExplosion(int damage, GameObject cause)
    {
        if (hs.CurrentHealth <= 0) Explode(cause);
    }

    public void Explode(GameObject cause)
    {
        ExplodeEffect(cause);
        if (explosion) Instantiate(explosion, transform.position, transform.rotation);
        if (explosionSound) sfxController.PlaySoundEffect(explosionSound);
        Destroy(gameObject);
    }

    public virtual void ExplodeEffect(GameObject cause)
    {
        generator.AsteroidDestroyed(this, cause);
    }

    private IEnumerator CheckDistance()
    {
        while (true)
        {
            yield return new WaitForSeconds(distanceCheckDelay);
            if (generator)
            {
                if (Vector3.Distance(transform.position, shipLocation.position) > generator.DespawnDistance)
                {
                    generator.AsteroidDestroyed(this, null);
                    Destroy(gameObject);
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
