using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleAsteroid : Asteroid
{
    [SerializeField] private GameObject personalizedSignal;

    private GlobalEventsHandler geh;
    private Signal signal;

    public new void Start()
    {
        base.Start();
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        /*
         * _signalCanBeAddedOnSpawn is a flag for controller objects to let them know they can
         * add a signal to this asteroid.
         * Signals are tied to loot logic so having the wrong signal or multiple signals on an
         * object could cause unexpected results.
         * For this reason, if an Asteroid is flagged its personalized signal will be ignored.
         */
        if (personalizedSignal)
        {
            signal = personalizedSignal.GetComponent<Signal>();
            if (!_signalCanBeAddedOnSpawn) signal.SetupSignal(gameObject, null, _radarShadow.GetComponentsInChildren<MeshRenderer>());
            else Destroy(personalizedSignal);
        }
    }

    public override void ExplodeEffect(GameObject cause)
    {
        geh.Invoke_OnAsteroidDestruction(this, cause);
    }
}
