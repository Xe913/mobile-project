using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Comet : Asteroid
{
    protected GlobalEventsHandler geh;

    public new void Start()
    {
        base.Start();
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
    }

    protected override void AddInitialForce()
    {
        transform.rotation = Quaternion.LookRotation(shipLocation.position - transform.position);
        base.AddInitialForce();
    }

    public override void ExplodeEffect(GameObject cause)
    {
        if (cause.CompareTag(constDict.ShipTag)) geh.Invoke_OnFreezeShip();
        base.ExplodeEffect(cause);
    }
}
