using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis { x, y, z }

public class LevelBound : MonoBehaviour
{
    [SerializeField] private GameObject warningSign;
    [SerializeField] private Axis checkDistanceWithAxis;
    [SerializeField] private float showWarningAtDistance = 100f;

    private Transform shipLocation;
    private Vector3 axisMask;
    private GameObject warning;
    private Vector3 warningPosition = Vector3.zero;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        shipLocation = GameObject.FindGameObjectWithTag(constDict.ShipTag).transform;
        warning = Instantiate(warningSign, transform.position, transform.rotation);
        warning.transform.SetParent(transform);
        SetupAxisMask();
        warning.SetActive(false);
    }

    public void Update()
    {
        warningPosition = Vector3.Scale(shipLocation.position, axisMask) + Vector3.Scale(transform.position, Vector3.one - axisMask);
        if (warning.activeSelf && Vector3.Distance(shipLocation.position, warningPosition) > showWarningAtDistance) warning.SetActive(false);
        if (!warning.activeSelf && Vector3.Distance(shipLocation.position, warningPosition) <= showWarningAtDistance) warning.SetActive(true);
        warning.transform.position = warningPosition;
    }

    private void SetupAxisMask()
    {
        switch (checkDistanceWithAxis)
        {
            case Axis.x:
                axisMask = Vector3.one - Vector3.right;
                break;
            case Axis.y:
                axisMask = Vector3.one - Vector3.up;
                break;
            case Axis.z:
                axisMask = Vector3.one - Vector3.forward;
                break;
        }
    }
}
