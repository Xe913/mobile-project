using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selfdestruct : MonoBehaviour
{
    [SerializeField] private float timer;
    [SerializeField] private bool startOnSpawn = true;

    void Start()
    {
        if (startOnSpawn) StartSelfdestructionCountdown();
    }

    public void StartSelfdestructionCountdown()
    {
        StartSelfdestructionCountdown(timer);
    }

    public void StartSelfdestructionCountdown(float timer)
    {
        StartCoroutine(SelfdestructionCountdown(timer));
    }

    private IEnumerator SelfdestructionCountdown(float timer)
    {
        yield return new WaitForSeconds(timer);
        Destroy(gameObject);
    }
}
