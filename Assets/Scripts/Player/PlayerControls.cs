using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour
{
    [SerializeField] private Transform playerBody;
    [SerializeField] private Transform playerCamera;
    [SerializeField] GameObject ControlSchemes;
    [SerializeField] protected bool clampHorizontalRotation;
    [SerializeField] protected bool clampVerticalRotation;
    [SerializeField] protected Vector2 minRotationRange;
    [SerializeField] protected Vector2 maxRotationRange;
    [SerializeField] private float autoPositionSpeed;
    [SerializeField] private float autoRotationSpeed;
    [Header("Editor Options")]
    [SerializeField] bool forceKeyboardMode;

    protected GlobalEventsHandler geh;

    BaseControlScheme _selectedControlScheme;
    public bool CanMove { get => _selectedControlScheme.CanMove; }
    public BaseControlScheme ControlScheme { get => _selectedControlScheme; }

    public void Awake()
    {
        _selectedControlScheme = SelectControlScheme();
        ControlSchemes.SendMessage("ModeSelected", _selectedControlScheme);
        Debug.Log("Selected Control Scheme: " + _selectedControlScheme);
        if (!_selectedControlScheme) Debug.Log("No compatible input system detected");
    }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        if (_selectedControlScheme) _selectedControlScheme.Setup(
            playerBody, playerCamera,
            clampHorizontalRotation, clampVerticalRotation,
            minRotationRange, maxRotationRange);
        geh.Add_OnGameWon(DisableMovement);
        geh.Add_OnGameLost(DisableMovement);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(DisableMovement);
            geh.Remove_OnGameLost(DisableMovement);
        }
    }

    private BaseControlScheme SelectControlScheme()
    {
        BaseControlScheme control;
        if (!forceKeyboardMode)
        {
            control = ControlSchemes.GetComponent<GyroControlScheme>();
            if (IsSupported(control)) return control;
            control = ControlSchemes.GetComponent<AccelControlScheme>();
            if (IsSupported(control)) return control;
            control = ControlSchemes.GetComponent<TouchscreenControlScheme>();
            if (IsSupported(control)) return control;
        }
        else
        {
            Debug.Log("Forcing Keyboard Controls");
        }
        control = ControlSchemes.GetComponent<KeyboardControlScheme>();
        if (IsSupported(control)) return control;
        return null;
    }

    public void MovePlayerCoroutine(Vector3 targetPosition, Quaternion targetRotation, bool reenableMovement = true, Action callWhenInPosition = null)
    {
        StartCoroutine(MovePlayerTo(targetPosition, targetRotation, reenableMovement, callWhenInPosition));
    }

    private IEnumerator MovePlayerTo(Vector3 finalPosition, Quaternion finalRotation, bool reenableMovement, Action callWhenInPosition)
    {
        DisableMovement();
        _selectedControlScheme.TargetRotation = Vector2.zero;
        bool positionReached = false;
        bool rotationReached = false;
        while(!positionReached || !rotationReached)
        {
            if (playerBody.position != finalPosition)
            {
                playerBody.position = Vector3.MoveTowards(playerBody.position, finalPosition, autoPositionSpeed * Time.deltaTime);
            }
            else
            {
                positionReached = true;
            }
            if (playerBody.rotation != finalRotation || playerCamera.rotation != finalRotation)
            {
                playerBody.rotation = Quaternion.RotateTowards(playerBody.rotation, finalRotation, autoRotationSpeed * Time.deltaTime);
                playerCamera.rotation = Quaternion.RotateTowards(playerCamera.rotation, finalRotation, autoRotationSpeed * Time.deltaTime);
            }
            else
            {
                rotationReached = true;
            }
            yield return null;
        }
        if (callWhenInPosition != null) callWhenInPosition.Invoke();
        if (reenableMovement) EnableMovement();
    }

    public void EnableMovement()
    {
        ControlScheme.EnableControls();
    }

    public void DisableMovement()
    {
        ControlScheme.DisableControls();
    }

    public void ResetRotation()
    {
        _selectedControlScheme.UpdateDefaultRotation();
        _selectedControlScheme.ReturnToOrigin();
    }

    public void ReturnToOrigin()
    {
        _selectedControlScheme.ReturnToOrigin();
    }

    public void UpdateDefaultRotation()
    {
        _selectedControlScheme.UpdateDefaultRotation();
    }

    private bool IsSupported(BaseControlScheme control) => control && control.enabled && control.IsSupported();
}
