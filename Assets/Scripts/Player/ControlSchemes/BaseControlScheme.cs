using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseControlScheme : MonoBehaviour
{
    [Header("Base Control Scheme Params")]
    [SerializeField] protected Vector2 rotationSpeed = Vector2.one;
    [SerializeField] protected bool overrideRotationRangeData;
    [Header("Override Params")]
    [SerializeField] protected bool clampHorizontalRotation;
    [SerializeField] protected bool clampVerticalRotation;
    [SerializeField] protected Vector2 minRotationRange;
    [SerializeField] protected Vector2 maxRotationRange;

    protected ConstDictionary constDict;
    protected Transform playerBody;
    protected Transform playerCamera;

    protected Vector2 rotationInput = Vector2.zero;
    protected Vector2 _targetRotation;
    protected Quaternion _defaultRotation;
    protected bool _canMove = false;

    public Vector2 RotationSpeed { get => rotationSpeed; set => rotationSpeed = value; }
    public Vector2 TargetRotation { get => _targetRotation; set => _targetRotation = value; }
    public Quaternion DefaultRotation { get => _defaultRotation; }
    public bool CanMove { get => _canMove; }

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
    }

    public void Setup(Transform playerBody, Transform playerCamera, bool clampHorizontalRotation, bool clampVerticalRotation, Vector2 minRotationRange, Vector2 maxRotationRange)
    {
        this.playerBody = playerBody;
        this.playerCamera = playerCamera;
        if (!overrideRotationRangeData)
        {
            this.clampHorizontalRotation = clampHorizontalRotation;
            this.clampVerticalRotation = clampVerticalRotation;
            this.minRotationRange = minRotationRange;
            this.maxRotationRange = maxRotationRange;
        }
        _canMove = true;
    }

    public void Update()
    {
        if (CanMove)
        {
            _targetRotation.x -= rotationInput.y * rotationSpeed.y * Time.deltaTime;
            _targetRotation.y += rotationInput.x * RotationSpeed.x * Time.deltaTime;
            if (clampHorizontalRotation) _targetRotation.y = Mathf.Clamp(_targetRotation.y, minRotationRange.x, maxRotationRange.x);
            if (clampVerticalRotation) _targetRotation.x = Mathf.Clamp(_targetRotation.x, minRotationRange.y, maxRotationRange.y);
            playerBody.rotation = Quaternion.Euler(0f, _targetRotation.y, 0f);
            playerCamera.localRotation = Quaternion.Euler(_targetRotation.x, 0f, 0f);
        }
    }

    /*
     * Could be private but its "called by" ControlSchemeSelector via SendMessage.
     * Making it public and considering it so logic-wise when writing the code
     * is a way to avoid breaking encapsulation.
     */
    public bool ModeSelected(BaseControlScheme selectedControlScheme)
    {
        if (selectedControlScheme.GetType() != this.GetType()) enabled = false;
        return enabled;
    }

    public void ReturnToOrigin()
    {
        playerBody.rotation = Quaternion.identity;
        playerCamera.rotation = Quaternion.identity;
        _targetRotation = Vector2.zero;
    }

    public virtual void EnableControls()
    {
        _canMove = true;
    }

    public virtual void DisableControls()
    {
        _canMove = false;
    }

    //used for motion controls
    public abstract void UpdateDefaultRotation();

    public abstract bool IsSupported();
}
