using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControlScheme : BaseControlScheme
{
    public new void Start()
    {
        base.Start();
    }

    public new bool ModeSelected(BaseControlScheme selectedControlScheme)
    {
        bool selected = base.ModeSelected(selectedControlScheme);
        Input.gyro.enabled = selected;
        return selected;
    }

    public override void UpdateDefaultRotation() { }

    public override bool IsSupported() => SystemInfo.supportsGyroscope;
}
