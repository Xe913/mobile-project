using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccelControlScheme : BaseControlScheme
{
    public new void Start()
    {
        base.Start();
    }

    public override void UpdateDefaultRotation() { }

    public override bool IsSupported() => SystemInfo.supportsAccelerometer;
}
