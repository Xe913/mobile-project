using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchscreenControlScheme : BaseControlScheme
{
    [Header("Touchscreen Control Scheme Params")]
    [SerializeField] private GameObject controlStick;

    private JoystickController joystickController;

    public new void Start()
    {
        base.Start();
        joystickController = Instantiate(
            controlStick,
            GameObject.FindGameObjectWithTag(constDict.PlayerControlsCanvas).transform
            ).GetComponent<JoystickController>();
        if (joystickController) joystickController.Add_OnJoystickInput(RotateOnJoystickInput);
    }

    public void OnDestroy()
    {
        if (joystickController) joystickController.Remove_OnJoystickInput(RotateOnJoystickInput);
    }

    private void RotateOnJoystickInput(Vector2 joystickInput)
    {
        rotationInput.x = joystickInput.x;
        rotationInput.y = joystickInput.y;
    }

    public override void EnableControls()
    {
        base.EnableControls();
        joystickController.gameObject.SetActive(true);
    }

    public override void DisableControls()
    {
        joystickController.gameObject.SetActive(false);
        base.DisableControls();
    }

    public override void UpdateDefaultRotation() { }

    public override bool IsSupported() => true;
}
