using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardControlScheme : BaseControlScheme
{
    public new void Update()
    {
        if (CanMove)
        {
            if (Input.GetKeyDown(KeyCode.W)) rotationInput.y += 1;
            if (Input.GetKeyUp(KeyCode.W)) rotationInput.y -= 1;
            if (Input.GetKeyDown(KeyCode.A)) rotationInput.x -= 1;
            if (Input.GetKeyUp(KeyCode.A)) rotationInput.x += 1;
            if (Input.GetKeyDown(KeyCode.S)) rotationInput.y -= 1;
            if (Input.GetKeyUp(KeyCode.S)) rotationInput.y += 1;
            if (Input.GetKeyDown(KeyCode.D)) rotationInput.x += 1;
            if (Input.GetKeyUp(KeyCode.D)) rotationInput.x -= 1;
        }
        base.Update();
    }

    //no need to update the default rotation when not using motion control
    public override void UpdateDefaultRotation() { }

    public override bool IsSupported() => true;
}
