using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private GlobalEventsHandler geh;
    [SerializeField] private bool _canInteract;

    public bool CanInteract { get => _canInteract; set => _canInteract = value; }

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        StartCoroutine(InitialCanInteract());
        geh.Add_OnGameWon(DisableInteraction);
        geh.Add_OnGameLost(DisableInteraction);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(DisableInteraction);
            geh.Remove_OnGameLost(DisableInteraction);
        }
    }

    private IEnumerator InitialCanInteract()
    {
        yield return new WaitForEndOfFrame();
        _canInteract = true;
    }

    private void DisableInteraction()
    {
        _canInteract = false;
    }
}
