using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairToolUIController : ContextualUIController
{
    private Player player;

    public new void Start()
    {
        base.Start();
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
    }

    protected override void AddToEvents()
    {
        geh.Add_OnRepairToolPickUp(EngageStart);
        geh.Add_OnRepairToolPutDown(DisengageStart);
    }

    protected override void RemoveFromEvents()
    {
        if (geh)
        {
            geh.Remove_OnRepairToolPickUp(EngageStart);
            geh.Remove_OnRepairToolPutDown(DisengageStart);
        }
    }

    protected override void EngageStart()
    {
        player.CanInteract = false;
        base.EngageStart();
    }

    protected override void EngageEnd()
    {
        base.EngageEnd();
        player.CanInteract = true;
    }

    protected override void DisengageStart()
    {
        player.CanInteract = false;
        base.DisengageStart();
    }

    protected override void DisengageEnd()
    {
        base.DisengageEnd();
    }
}
