using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PutDownRepairToolButton : MonoBehaviour
{
    private GlobalEventsHandler geh;
    private Player player;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
    }

    public void PutDownButton()
    {
        geh.Invoke_OnRepairToolPutDown();
        player.CanInteract = true;
    }
}
