using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingObjectLocationCycler : Interactable
{
    [SerializeField] private MovingObject controlledObject;

    public new void Start()
    {
        base.Start();
        controlledObject.Add_OnNextLocationReached(EnableInteraction);
    }

    public void OnDestroy()
    {
        if (controlledObject) controlledObject.Remove_OnNextLocationReached(EnableInteraction);
    }

    protected override bool InteractEffect()
    {
        if (!controlledObject.Moving)
        {
            DisableInteraction();
            controlledObject.GoToNextLocation();
            return true;
        }
        return false;
    }
}
