using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchViewButton : Interactable
{
    [SerializeField] private Material _viewMaterial;
    [SerializeField] private Material activeButtonMaterial;

    private Action<SwitchViewButton> OnSwitchView;

    private MeshRenderer mr;
    private Material defaultMaterial;

    public Material ViewMaterial { get => _viewMaterial; }

    public void Awake()
    {
        mr = GetComponent<MeshRenderer>();
        defaultMaterial = gameObject.GetComponent<MeshRenderer>().material;
    }

    protected override bool InteractEffect()
    {
        Invoke_OnSwitchView();
        return true;
    }

    public void SetAsActiveButton()
    {
        mr.material = activeButtonMaterial;
    }

    public void SetAsDefaultButton()
    {
        mr.material = defaultMaterial;
    }

    public void Add_OnSwitchView(Action<SwitchViewButton> listener)
    {
        OnSwitchView += listener;
    }

    public void Remove_OnSwitchView(Action<SwitchViewButton> listener)
    {
        if (OnSwitchView != null) OnSwitchView -= listener;
    }

    private void Invoke_OnSwitchView()
    {
        if (OnSwitchView != null) OnSwitchView.Invoke(this);
    }
}
