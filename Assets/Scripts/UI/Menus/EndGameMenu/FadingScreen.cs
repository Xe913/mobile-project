using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadingScreen : MonoBehaviour
{
    [SerializeField] private string FadeOutTrigger = "FadeOut";
    private GlobalEventsHandler geh;
    private Image fadingImage;
    private Animator animator;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        fadingImage = GetComponent<Image>();
        animator = GetComponent<Animator>();
        geh.Add_OnGameWon(FadeToBlack);
        geh.Add_OnGameLost(FadeToBlack);
    }

    public void OnDestroy()
    {
        geh.Remove_OnGameWon(FadeToBlack);
        geh.Remove_OnGameLost(FadeToBlack);
    }

    private void DisableWhenFadeInEnds()
    {
        fadingImage.enabled = false;
    }

    private void FadeToBlack()
    {
        fadingImage.enabled = true;
        animator.SetTrigger(FadeOutTrigger);
    }

    private void TurnBlack()
    {
        fadingImage.color = Color.black;
        Time.timeScale = 0;
    }
}
