using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EndGameCanvasController : MonoBehaviour
{
    [SerializeField] private string winMessage = "YOU WON";
    [SerializeField] private string loseMessage = "YOU LOST";
    [SerializeField] private GameObject endGameMenu;
    [SerializeField] private TextMeshProUGUI winLoseText;
    [SerializeField] private TextMeshProUGUI timeText;

    private GlobalEventsHandler geh;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        endGameMenu.SetActive(false);
        geh.Add_OnGameWon(GameWon);
        geh.Add_OnGameLost(GameLost);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(GameWon);
            geh.Remove_OnGameLost(GameLost);
        }
    }

    public void GameWon()
    {
        OpenEndGameMenu(winMessage);
    }

    public void GameLost()
    {
        OpenEndGameMenu(loseMessage);
    }

    private void OpenEndGameMenu(string message)
    {
        TextMeshProUGUI endMessage = winLoseText.GetComponent<TextMeshProUGUI>();
        if (endMessage) endMessage.text = message;
        if (timeText) timeText.text = SecondsToTimeString(Time.timeSinceLevelLoad);
        endGameMenu.SetActive(true);
    }

    private string SecondsToTimeString(float seconds)
    {
        Debug.Log("seconds" + seconds);
        if (seconds / 60f >= 1000) return "99:59";
        int secs = Mathf.FloorToInt(seconds % 60);
        string time = "";
        if (Mathf.FloorToInt(seconds / 60) < 10) time += "0";
        time += Mathf.FloorToInt(seconds / 60) + ":";
        if (Mathf.FloorToInt(seconds % 60) < 10) time += "0";
        time += Mathf.FloorToInt(seconds % 60);
        return time;
    }
}
