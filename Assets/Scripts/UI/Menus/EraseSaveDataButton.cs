using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EraseSaveDataButton : MonoBehaviour
{
    [SerializeField] private GameObject menu;
    private SaveManager sm;

    public void Start()
    {
        sm = GameObject.FindGameObjectWithTag("GameController").GetComponent<SaveManager>();
    }

    public void EraseSaveData()
    {
        sm.ClearSaveData();
        menu.SetActive(false);
    }
}
