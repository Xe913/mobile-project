using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DifficutlyButton : MonoBehaviour
{
    private OptionsController options;
    private TextMeshProUGUI difficultyText;

    public void Start()
    {
        options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        UpdateText();
    }

    private void OnEnable()
    {
        UpdateText();
    }

    public void ChangeDifficulty()
    {
        options.UpdateDifficulty((DifficultySettings)(((int)options.Difficulty + 1) % (int)DifficultySettings.Count));
        UpdateText();
    }

    private void UpdateText()
    {
        if (!difficultyText) difficultyText = GetComponentInChildren<TextMeshProUGUI>();
        if(!options) options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        difficultyText.text = options.Difficulty.ToString();
    }
}
