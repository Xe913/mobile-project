using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class VolumeSlider : MonoBehaviour
{
    protected OptionsController options;
    protected Slider slider;

    public void Start()
    {
        options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        UpdateSlider();
        slider.minValue = 0;
        slider.maxValue = 1;
    }

    private void OnEnable()
    {
        UpdateSlider();
    }

    private void UpdateSlider()
    {
        if (!slider) slider = GetComponent<Slider>();
        if (!options) options = GameObject.FindGameObjectWithTag("GameController").GetComponent<OptionsController>();
        slider.value = GetVolumeFromOptions();
    }

    public abstract void UpdateVolume();

    protected abstract float GetVolumeFromOptions();
}
