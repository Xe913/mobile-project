using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgMusicSlider : VolumeSlider
{
    public override void UpdateVolume()
    {
        options.UpdateBgMusicVolume(slider.value);
    }

    protected override float GetVolumeFromOptions()
    {
        return options.BgMusicVolume;
    }
}
