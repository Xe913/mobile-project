using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectsSlider : VolumeSlider
{
    public override void UpdateVolume()
    {
        options.UpdateSoundEffectsVolume(slider.value);
    }

    protected override float GetVolumeFromOptions()
    {
        return options.SoundEffectsVolume;
    }
}
