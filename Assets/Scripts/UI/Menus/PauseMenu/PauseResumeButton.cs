using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseResumeButton : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;

    private ConstDictionary constDict;
    private Player player;
    private PlayerControls playerControls;
    private float defaultTimeScale = 1;
    private bool interactionStateBeforePause;
    private bool movementStateBeforePause;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
        playerControls = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<PlayerControls>();
        defaultTimeScale = constDict.DefaultTimeScale;
        pauseMenu.SetActive(false);
    }

    public void PauseResume()
    {
        if (Time.timeScale > 0) Pause();
        else Resume();
    }

    private void Pause()
    {
        interactionStateBeforePause = player.CanInteract;
        player.CanInteract = false;
        movementStateBeforePause = playerControls.CanMove;
        if (movementStateBeforePause) playerControls.DisableMovement();
        Time.timeScale = 0;
        pauseMenu.SetActive(true);
    }

    private void Resume()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = defaultTimeScale;
        player.CanInteract = interactionStateBeforePause;
        if (movementStateBeforePause) playerControls.EnableMovement();
    }
}
