using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackToMainMenuButton : MonoBehaviour
{
    private ConstDictionary constDict;
    private SceneLoader sl;

    void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sl = GameObject.FindGameObjectWithTag("GameController").GetComponent<SceneLoader>();
    }

    public void GoToMainMenu()
    {
        Time.timeScale = constDict.DefaultTimeScale;
        sl.LoadMainMenu();
    }
}
