using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenCloseMenuButton : MonoBehaviour
{
    [SerializeField] private GameObject menu;

    public void Start()
    {
        CloseMenu();
    }

    public void OpenMenu()
    {
        menu.SetActive(true);
    }

    public void CloseMenu()
    {
        menu.SetActive(false);
    }
}
