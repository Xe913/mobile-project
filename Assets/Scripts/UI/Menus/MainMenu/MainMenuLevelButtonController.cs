using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenuLevelButtonController : MonoBehaviour
{
    [SerializeField] private GameObject levelNotCompletedButton;
    [SerializeField] private GameObject levelCompletedButton;
    [SerializeField] private int levelNumber;
    [SerializeField] private string customName;

    private ConstDictionary constDict;
    private SaveManager sm;
    private LoadLevelButton loadLevelNotCompletedButton;
    private LoadLevelButton loadLevelCompletedButton;

    void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        sm = GameObject.FindGameObjectWithTag("GameController").GetComponent<SaveManager>();
        loadLevelNotCompletedButton = levelNotCompletedButton.GetComponent<LoadLevelButton>();
        loadLevelCompletedButton = levelCompletedButton.GetComponent<LoadLevelButton>();
        sm.Add_OnClearData(SetupCorrectButton);
        SetupCorrectButton();
    }

    public void OnDestroy()
    {
        if (sm) sm.Remove_OnClearData(SetupCorrectButton);
    }

    private void SetupCorrectButton()
    {
        int bestTime = sm.LoadLevelData(levelNumber);
        if (bestTime < 0)
        {
            if (loadLevelNotCompletedButton) loadLevelNotCompletedButton.SetupButton(levelNumber, GetLevelName(), bestTime, false);
            levelNotCompletedButton.SetActive(true);
            levelCompletedButton.SetActive(false);
        }
        else
        {
            if (loadLevelCompletedButton) loadLevelCompletedButton.SetupButton(levelNumber, GetLevelName(), bestTime, true);
            levelNotCompletedButton.SetActive(false);
            levelCompletedButton.SetActive(true);
        }
    }

    private string GetLevelName()
    {
        return customName.Length > 0 ? customName : constDict.LevelScenePrefix + " " + levelNumber.ToString();
    }
}
