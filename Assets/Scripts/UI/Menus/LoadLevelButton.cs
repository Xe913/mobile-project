using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadLevelButton : MonoBehaviour
{
    [SerializeField] private GameObject nameText;
    [SerializeField] private GameObject bestTimeText;

    private SceneLoader sl;
    private TextMeshProUGUI levelNameText;
    private TextMeshProUGUI levelBestTimeText;
    private int levelNumber;
    private bool displayTime;

    public void SetupButton(int levelNumber, string levelName, int bestTime, bool displayTime)
    {
        sl = GameObject.FindGameObjectWithTag("GameController").GetComponent<SceneLoader>();
        if (nameText) levelNameText = nameText.GetComponent<TextMeshProUGUI>();
        if (bestTimeText) levelBestTimeText = bestTimeText.GetComponent<TextMeshProUGUI>();
        this.levelNumber = levelNumber;
        SetupLevelName(levelName);
        SetupBestTimeDisplay(bestTime, displayTime);
    }

    private void SetupLevelName(string name)
    {
        if (levelNameText) levelNameText.text = name;
    }

    private void SetupBestTimeDisplay(int timeInSeconds, bool display)
    {
        if (levelBestTimeText && timeInSeconds < 0) this.displayTime = false;
        else displayTime = display;
        if (displayTime) levelBestTimeText.text = SecondsToTimeString(timeInSeconds);
    }

    private string SecondsToTimeString(int seconds)
    {
        if (seconds / 60f >= 1000) return "99:59";
        int secs = Mathf.FloorToInt(seconds % 60);
        string time = "";
        if (Mathf.FloorToInt(seconds / 60) < 10) time += "0";
        time += Mathf.FloorToInt(seconds / 60) + ":";
        if (Mathf.FloorToInt(seconds % 60) < 10) time += "0";
        time += Mathf.FloorToInt(seconds % 60);
        return time;
    }

    public void LoadLevel()
    {
        sl.LoadLevel(levelNumber);
    }
}
