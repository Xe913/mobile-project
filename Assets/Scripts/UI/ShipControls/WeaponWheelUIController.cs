using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponWheelUIController : ShipUIController
{
    private DisplaySelector wheel;

    public new void Start()
    {
        base.Start();
        wheel = GetComponentInChildren<DisplaySelector>();
    }

    protected override void EngageStart()
    {
        base.EngageStart();
        wheel.EnableActiveDisplay();
    }
}
