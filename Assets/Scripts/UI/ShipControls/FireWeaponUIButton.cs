using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FireWeaponUIButton : WeaponFireController, IPointerDownHandler, IPointerUpHandler
{
    private bool active = true;

    public new void Start()
    {
        base.Start();
        geh.Add_OnWeaponSwapInput(DeactivateOnSwapInput);
        geh.Add_OnWeaponSwapped(ActivateWhenSwapped);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnWeaponSwapInput(DeactivateOnSwapInput);
            geh.Remove_OnWeaponSwapped(ActivateWhenSwapped);
        }
    }

    protected override void InvokeStartFiringEvents()
    {
        if (active) geh.Invoke_OnStartFiringShipWeapon();
    }

    protected override void InvokeStopFiringEvents()
    {
        if (active) geh.Invoke_OnStopFiringShipWeapon();
    }

    private void DeactivateOnSwapInput(SwapDirection direction)
    {
        active = false;
        InvokeStopFiringEvents();
    }

    private void ActivateWhenSwapped(Weapon activeWeapon)
    {
        active = true;
    }
}
