using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlsManager : MonoBehaviour
{
    [SerializeField] private Transform shipControlsViewLocation;

    private GlobalEventsHandler geh;
    private Player player;
    private PlayerControls pc;
    [SerializeField] private int registeredControls;
    [SerializeField] private int engagedControls;

    void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        player = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<Player>();
        pc = GameObject.FindGameObjectWithTag(constDict.PlayerTag).GetComponent<PlayerControls>();
        geh.Add_OnRequestShipControlsEngage(EngageShipControls);
    }

    public void OnDestroy()
    {
        if (geh) geh.Remove_OnRequestShipControlsEngage(EngageShipControls);
    }

    private void EngageShipControls()
    {
        pc.MovePlayerCoroutine(shipControlsViewLocation.position, shipControlsViewLocation.rotation, false);
    }

    public void ControlEngaged()
    {
        engagedControls = Mathf.Min(engagedControls + 1, registeredControls);
        if (engagedControls == registeredControls)
        {
            geh.Invoke_OnShipControlsEngaged();
            player.CanInteract = true;
        }

    }

    public void ControlDisengaged()
    {
        engagedControls = Mathf.Max(engagedControls - 1, 0);
        if (engagedControls == 0)
        {
            player.CanInteract = true;
        }
    }

    public void RegisterControl()
    {
        registeredControls++;
    }
}
