using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DistanceText : MonoBehaviour
{
    [SerializeField] private float displayedRangeMultiplier = 1000f;
    [SerializeField] private Color inRangeColor;
    [SerializeField] private Color outOfRangeColor;
    [SerializeField] private AimController aimInfo;
    [SerializeField] private Weapon syncToWeapon;
    [SerializeField] private bool syncToCurrentWeapon;

    private GlobalEventsHandler geh;
    [SerializeField] private TextMeshProUGUI displayText;
    [SerializeField] private float weaponRange = 0;

    private bool visible;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        //displayText = GetComponent<TextMeshProUGUI>();
        displayText.color = outOfRangeColor;
        geh.Add_OnShipControlsEngaged(Show);
        geh.Add_OnRequestShipControlsDisengage(Hide);
        if (syncToCurrentWeapon)
        {
            syncToWeapon = null;
            geh.Add_OnWeaponSwapped(Sync);
        }
        else
        {
            Sync(syncToWeapon);
        }
        Hide();
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnShipControlsEngaged(Show);
            geh.Remove_OnRequestShipControlsDisengage(Hide);
            if (syncToCurrentWeapon) geh.Remove_OnWeaponSwapped(Sync);
        }
    }

    private bool wasInRange = false;
    public void Update()
    {
        if (visible)
        {
            if (!wasInRange)
            {
                if (aimInfo.AimedObjectDistance >= 0 && aimInfo.AimedObjectDistance <= weaponRange)
                {
                    displayText.color = inRangeColor;
                    wasInRange = true;
                }
            }
            else
            {
                if (aimInfo.AimedObjectDistance < 0 || aimInfo.AimedObjectDistance >= weaponRange)
                {
                    displayText.color = outOfRangeColor;
                    wasInRange = false;
                }
            }
            if (aimInfo.AimedObjectDistance >= 0)
            {
                displayText.text = (aimInfo.AimedObjectDistance * displayedRangeMultiplier).ToString("0000:00");
            }
            else
            {
                displayText.text = "----.--";
                wasInRange = false;
            }
        }
    }

    private void Sync(Weapon weapon)
    {
        weaponRange = weapon.Range;
    }

    private void Show()
    {
        //if (!displayText) displayText = GetComponent<TextMeshProUGUI>();
        visible = true;
        Color alphaMax = displayText.color;
        alphaMax.a = 1;
        displayText.color = alphaMax;
    }

    private void Hide()
    {
        //if (!displayText) displayText = GetComponent<TextMeshProUGUI>();
        visible = false;
        Color alphaMin = displayText.color;
        alphaMin.a = 0;
        displayText.color = alphaMin;
    }
}
