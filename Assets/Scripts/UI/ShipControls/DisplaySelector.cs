using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisplaySelector : MonoBehaviour
{
    [SerializeField] private MovingObject movingObject;
    [SerializeField] private Transform rotatingComponent;
    [SerializeField] private GameObject[] displays;

    private GlobalEventsHandler geh;
    private WeaponController weaponController;
    private int _activeSegment = 0;
    private bool canRotate = true;
    private MeshRenderer[] displayTexts;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        weaponController = GameObject.FindGameObjectWithTag(constDict.ShipTag).GetComponentInChildren<WeaponController>();
        displayTexts = new MeshRenderer[displays.Length];
        for (int i = 0; i < displays.Length; i++) displayTexts[i] = displays[i].GetComponent<MeshRenderer>();
        movingObject.Add_OnNextLocationReached(ToNextWeapon);
        movingObject.Add_OnPreviousLocationReached(ToPreviousWeapon);
        geh.Add_OnWeaponSwapInput(Rotate);
    }

    public void OnDestroy()
    {
        if (movingObject)
        {
            movingObject.Remove_OnNextLocationReached(ToNextWeapon);
            movingObject.Remove_OnPreviousLocationReached(ToPreviousWeapon);
        }
        if (geh) geh.Remove_OnWeaponSwapInput(Rotate);
    }

    public void EnableActiveDisplay()
    {
        for (int i = 0; i < displayTexts.Length; i++) displayTexts[i].enabled = false;
        displayTexts[_activeSegment].enabled = true;
    }

    private void Rotate(SwapDirection direction)
    {
        if (canRotate)
        {
            canRotate = false;
            displayTexts[_activeSegment].enabled = false;
            switch (direction)
            {
                case SwapDirection.next:
                    movingObject.GoToNextLocation();
                    break;
                case SwapDirection.previous:
                    movingObject.GoToPreviousLocation();
                    break;
            }
        }
    }

    private void ToNextWeapon()
    {
        _activeSegment = weaponController.ActivetWeaponIndex;
        displayTexts[_activeSegment].enabled = true;
        weaponController.CompleteSwap();
        canRotate = true;
    }

    private void ToPreviousWeapon()
    {
        _activeSegment = weaponController.ActivetWeaponIndex;
        displayTexts[_activeSegment].enabled = true;
        weaponController.CompleteSwap();
        canRotate = true;
    }
}
