using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class JoystickInputHandler : MonoBehaviour
{
    protected ConstDictionary constDict;
    protected GlobalEventsHandler geh;
    protected JoystickController joystickController;

    public void Start()
    {
        constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        joystickController = GetComponent<JoystickController>();
        joystickController.Add_OnJoystickInput(JoystickInputEvent);
        joystickController.Add_OnInputStart(InputStartEvent);
        joystickController.Add_OnInputEnd(InputEndEvent);
    }

    public void OnDestroy()
    {
        if (joystickController)
        {
            joystickController.Remove_OnJoystickInput(JoystickInputEvent);
            joystickController.Remove_OnInputStart(InputStartEvent);
            joystickController.Remove_OnInputEnd(InputEndEvent);
        }
    }

    protected virtual void JoystickInputEvent(Vector2 joystickInput) { }

    protected virtual void InputStartEvent(Vector2 joysticNeutralPosition) { }

    protected virtual void InputEndEvent(Vector2 joystickNeutralPosition) { }
}
