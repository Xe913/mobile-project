using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipRotationInputHandler : JoystickInputHandler
{
    protected override void InputStartEvent(Vector2 joysticNeutralPosition)
    {
        geh.Invoke_OnShipRotationInput(Vector2.zero);
    }

    protected override void JoystickInputEvent(Vector2 joystickInput)
    {
        geh.Invoke_OnShipRotationInput(joystickInput);
    }
}
