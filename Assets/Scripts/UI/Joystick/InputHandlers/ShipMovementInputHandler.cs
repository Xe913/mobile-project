using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovementInputHandler : JoystickInputHandler
{
    protected override void JoystickInputEvent(Vector2 joystickInput)
    {
        geh.Invoke_OnShipMovementInput(joystickInput.y);
    }
}
