using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSelectionInputHandler : JoystickInputHandler
{
    [Range(0, 1)]
    [SerializeField] private float wheelThreshold = 0.5f;

    private bool selectionDone = false;

    protected override void JoystickInputEvent(Vector2 joystickInput)
    {
        if (!selectionDone)
        {
            if (joystickInput.y > wheelThreshold)
            {
                geh.Invoke_OnWeaponSwapInput(SwapDirection.next);
                selectionDone = true;
            }
            if (joystickInput.y < -wheelThreshold)
            {
                geh.Invoke_OnWeaponSwapInput(SwapDirection.previous);
                selectionDone = true;
            }
        }
    }

    protected override void InputEndEvent(Vector2 joystickNeutralPosition)
    {
        selectionDone = false;
    }
}
