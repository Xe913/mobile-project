using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickController : MonoBehaviour, IPointerDownHandler, IDragHandler, IPointerUpHandler
{
    private GlobalEventsHandler geh;
    private Joystick joystick;

    private Action<Vector2> onInputStart;
    private Action<Vector2> onInputEnd;

    public void Start()
    {
        ConstDictionary constDict = GameObject.FindGameObjectWithTag("GameController").GetComponent<ConstManager>().ConstDict;
        geh = GameObject.FindGameObjectWithTag(constDict.GlobalEventHandlerTag).GetComponent<GlobalEventsHandler>();
        SetupJoystick();
        geh.Add_OnGameWon(DisableJoystick);
        geh.Add_OnGameLost(DisableJoystick);
    }

    public void OnDestroy()
    {
        if (geh)
        {
            geh.Remove_OnGameWon(DisableJoystick);
            geh.Remove_OnGameLost(DisableJoystick);
        }
    }

    public void SetupJoystick()
    {
        if (!joystick)
        {
            joystick = GetComponentInChildren<Joystick>();
            joystick.gameObject.SetActive(false);
        }
    }

    public void DisableJoystick()
    {
        gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        joystick.gameObject.SetActive(true);
        joystick.StartJoystick(eventData.position);
        Invoke_OnInputStart(eventData.position);
    }

    public void OnDrag(PointerEventData eventData)
    {
        joystick.StickMovement(eventData.position);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        joystick.ResetJoystick();
        joystick.gameObject.SetActive(false);
        Invoke_OnInputEnd(eventData.position);
    }

    public void Add_OnInputStart(Action<Vector2> listener)
    {
        onInputStart += listener;
    }

    public void Remove_OnInputStart(Action<Vector2> listener)
    {
        if (onInputStart != null) onInputStart -= listener;
    }

    public void Invoke_OnInputStart(Vector2 joystickNeutralPosition)
    {
        if (onInputStart != null) onInputStart.Invoke(joystickNeutralPosition);
    }

    public void Add_OnInputEnd(Action<Vector2> listener)
    {
        onInputEnd += listener;
    }

    public void Remove_OnInputEnd(Action<Vector2> listener)
    {
        if (onInputEnd != null) onInputEnd -= listener;
    }

    public void Invoke_OnInputEnd(Vector2 joystickNeutralPosition)
    {
        if (onInputEnd != null) onInputEnd.Invoke(joystickNeutralPosition);
    }

    public void Add_OnJoystickInput(Action<Vector2> listener)
    {
        SetupJoystick();
        joystick.Add_OnJoystickInput(listener);
    }

    public void Remove_OnJoystickInput(Action<Vector2> listener)
    {
        joystick.Remove_OnJoystickInput(listener);
    }
}
