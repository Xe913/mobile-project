using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Joystick : MonoBehaviour
{
    [SerializeField] protected GameObject movingStick;
    [Range(0.01f, 0.1f)]
    [SerializeField] protected float movingStickRange = 0.03f;

    protected GlobalEventsHandler geh;
    protected Action<Vector2> onJoystickInput;

    public void Start()
    {
        geh = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalEventsHandler>();
        movingStickRange = movingStickRange * Screen.width;
    }

    public void StartJoystick(Vector2 initPosition)
    {
        transform.position = initPosition;
    }

    public virtual void ResetJoystick()
    {
        movingStick.transform.position = transform.position;
        JoystickMovementEffect(Vector2.zero);
    }

    private Vector2 relativeInputPosition;
    private Vector2 maxStickRangePosition;
    private Vector2 clampedPosition;
    public void StickMovement(Vector2 position)
    {
        relativeInputPosition = position - (Vector2)transform.position;
        maxStickRangePosition = relativeInputPosition.normalized * movingStickRange;
        clampedPosition = ClampPosition(relativeInputPosition, maxStickRangePosition);
        movingStick.transform.position = (Vector2)transform.position + clampedPosition;
        JoystickMovementEffect(AxisOutput(clampedPosition));
    }

    private Vector2 ClampPosition(Vector2 firstPosition, Vector2 secondPosition)
    {
        if (firstPosition.magnitude < secondPosition.magnitude) return firstPosition;
        else return secondPosition;
    }

    protected Vector2 AxisOutput(Vector2 position)
    {
        return position / (Vector2.one * movingStickRange);
    }

    protected void JoystickMovementEffect(Vector2 position)
    {
        Invoke_OnJoystickInput(position);
    }

    public void Add_OnJoystickInput(Action<Vector2> listener)
    {
        onJoystickInput += listener;
    }
    public void Remove_OnJoystickInput(Action<Vector2> listener)
    {
        if (onJoystickInput != null) onJoystickInput -= listener;
    }
    public void Invoke_OnJoystickInput(Vector2 input)
    {
        if (onJoystickInput != null) onJoystickInput.Invoke(input);
    }
}
