using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipUIController : ContextualUIController
{
    [SerializeField] private ShipControlsManager manager;

    public new void Start()
    {
        base.Start();
        manager.RegisterControl();
    }

    protected override void AddToEvents()
    {
        geh.Add_OnRequestShipControlsEngage(EngageStart);
        geh.Add_OnRequestShipControlsDisengage(DisengageStart);
    }

    protected override void RemoveFromEvents()
    {
        if (geh)
        {
            geh.Remove_OnRequestShipControlsEngage(EngageStart);
            geh.Remove_OnRequestShipControlsDisengage(DisengageStart);
        }
    }
    protected override void EngageEnd()
    {
        manager.ControlEngaged();
        base.EngageEnd();
    }

    protected override void DisengageEnd()
    {
        base.DisengageEnd();
        manager.ControlDisengaged();
    }
}
