using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipControlsUIManager : MonoBehaviour
{
    private GlobalEventsHandler geh;
    private List<GameObject> uiElements;

    public void Start()
    {
        geh = GameObject.FindGameObjectWithTag("GameController").GetComponent<GlobalEventsHandler>();
        uiElements = new List<GameObject>();
        foreach(Transform child in transform) uiElements.Add(child.gameObject);
        geh.Add_OnRequestShipControlsEngage(EnableUI);
        geh.Add_OnRequestShipControlsDisengage(DisableUI);
        StartCoroutine(InitialDisable());
    }

    public void OnDestroy()
    {
        geh.Remove_OnRequestShipControlsEngage(EnableUI);
        geh.Remove_OnRequestShipControlsDisengage(DisableUI);
    }

    private IEnumerator InitialDisable()
    {
        yield return new WaitForEndOfFrame();
        DisableUI();
    }

    private void EnableUI()
    {
        foreach (GameObject uiElement in uiElements) uiElement.SetActive(true);
    }

    private void DisableUI()
    {
        foreach (GameObject uiElement in uiElements) uiElement.SetActive(false);
    }
}
